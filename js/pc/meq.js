jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .divider'));
		$.fadeUpAn($('.sec1 .box-title1-2'));
		$.fadeUpAn($('.sec1 .content1'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .subtitle4'));
		$.fadeUpAn($('.sec5 .box-title5'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec1 .box-title1'));
			$.fadeUpAn($('.sec1 .divider'));
			$.fadeUpAn($('.sec1 .box-title1-2'));
			$.fadeUpAn($('.sec1 .content1'));
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .subtitle4'));
			$.fadeUpAn($('.sec5 .box-title5'));
		});
	}, 300);

	// sec1
	// scroll to 1
	$('.sec1 .cont1').on('click', function() {
		$('html, body').animate({
			scrollTop: ($('.sec2').offset().top - 94),
		}, 200);
	}); 
	// scroll to 2
	$('.sec1 .cont2').on('click', function() {
		$('html, body').animate({
			scrollTop: ($('.sec3').offset().top - 94),
		}, 400);
	});
	// scroll to 3
	$('.sec1 .cont3').on('click', function() {
		$('html, body').animate({
			scrollTop: ($('.sec4').offset().top - 94),
		}, 600);
	});
	// scroll to 4
	$('.sec1 .cont4').on('click', function() {
		$('html, body').animate({
			scrollTop: ($('.sec5').offset().top - 94),
		}, 800);
	});
});