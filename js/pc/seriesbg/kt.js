jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .text2-1'));
		$.fadeUpAn($('.sec2 .text2-2'));
		$.fadeUpAn($('.sec2 .box-swiper'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .box-effect'));
		$.fadeUpAn($('.sec3 .text3-1'));
		$.fadeUpAn($('.sec3 .text3-2'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .text4'));
		$.fadeUpAn($('.sec4 .card4-1'));
		$.fadeUpAn($('.sec4 .card4-2'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .prod5'));
		$.fadeUpAn($('.sec5 .text5-2-1'));
		$.fadeUpAn($('.sec6 .box-title6'));
		$.fadeUpAn($('.sec6 .text6'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .text2-1'));
			$.fadeUpAn($('.sec2 .text2-2'));
			$.fadeUpAn($('.sec2 .box-swiper'));
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .box-effect'));
			$.fadeUpAn($('.sec3 .text3-1'));
			$.fadeUpAn($('.sec3 .text3-2'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .text4'));
			$.fadeUpAn($('.sec4 .card4-1'));
			$.fadeUpAn($('.sec4 .card4-2'));
			$.fadeUpAn($('.sec5 .box-title5'));
			$.fadeUpAn($('.sec5 .prod5'));
			$.fadeUpAn($('.sec5 .text5-2-1'));
			$.fadeUpAn($('.sec6 .box-title6'));
			$.fadeUpAn($('.sec6 .text6'));
		});
	}, 300);

	// sec1
	// download
	$('.box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// 切换产品 初始值是1，第一个的颜色
	$('.sec1 .color-item' + $('.sec1 .box-prod img').data('index')).addClass('active');
	$('.sec1 .color-item').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec1 .box-prod img').data('index', $(this).data('color'));
		$('.sec1 .box-prod img').attr('src', 'img/pc/seriesbg/kt/prod1-' + $(this).data('color') + '.png');
	});
	// 180 check
	$.prod180An('box-kt', $('#box-kt'), '3d/180/bg/KT/0_', 1, 37);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-kt').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec2
	// swiper
	var swiper2 = new Swiper('.sec2 .swiper2', {
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 86, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton: '.sec2 .swiper-button-prev',
		nextButton: '.sec2 .swiper-button-next',
		pagination: '.sec2 .swiper-pagination',
		paginationType : 'progress'
	});

	// sec3 
	// wind effect
	function windAn3() {
		if($.visionAn($('.sec3 .box-effect'), 0.7, 1)) {
			$('.sec3 .box-wind').addClass('windAn3');
		};
	};
	windAn3();
	$(window).on('scroll', function() {
		windAn3();
	});

	// sec4
	// wind effect
	function windAn4() {
		if($.visionAn($('.sec4 .box-effect'), 0.8, 1)) {
			$('.sec4 .box-wind').addClass('windAn4');
		};
	};
	windAn4();
	$(window).on('scroll', function() {
		windAn4();
	});

	// sec5
	// text effect
	function textAn() {
		if($.visionAn($('.sec5 .text5-1'), 0.8, 1)) {
			$('.sec5 .text5-1-1').addClass('animated fadeInUp');
			setTimeout(function() {
				$('.sec5 .text5-1-2').addClass('animated fadeInUp');
			}, 500);
			setTimeout(function() {
				$('.sec5 .text5-1-3').addClass('animated fadeInUp');
			}, 1000);
			setTimeout(function() {
				$('.sec5 .text5-1-4').addClass('animated fadeInUp');
			}, 1500);
			setTimeout(function() {
				$('.sec5 .text5-1-5').addClass('animated fadeInUp');
			}, 2000);
			setTimeout(function() {
				$('.sec5 .text5-1-6').addClass('animated fadeInUp');
			}, 2500);
		};
	};
	textAn()
	$(window).on('scroll', function() {
		textAn();
	});
});