jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .prod2'));
		$.fadeUpAn($('.sec2 .text2-1'));
		$.fadeUpAn($('.sec2 .text2-2'));
		$.fadeUpAn($('.sec2 .text2-3'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .text3-1-1'));
		$.fadeUpAn($('.sec3 .text3-1-2'));
		$.fadeUpAn($('.sec3 .box-option'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .text4-1'));
		$.fadeUpAn($('.sec4 .text4-2'));
		$.fadeUpAn($('.sec4 .box-swiper'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .prod2'));
			$.fadeUpAn($('.sec2 .text2-1'));
			$.fadeUpAn($('.sec2 .text2-2'));
			$.fadeUpAn($('.sec2 .text2-3'));
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .text3-1-1'));
			$.fadeUpAn($('.sec3 .text3-1-2'));
			$.fadeUpAn($('.sec3 .box-option'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .text4-1'));
			$.fadeUpAn($('.sec4 .text4-2'));
			$.fadeUpAn($('.sec4 .box-swiper'));
		});
	}, 300);

	// sec1
	// download
	$('.box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// 切换产品 初始值是1，第一个的颜色
	$('.sec1 .color-item' + $('.sec1 .box-prod img').data('index')).addClass('active');
	$('.sec1 .color-item').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec1 .box-prod img').data('index', $(this).data('color'));
		$('.sec1 .box-prod img').attr('src', 'img/pc/seriesbg/wgj/prod1-' + $(this).data('color') + '.png');
	});
	// 180 check
	$.prod180An('box-wgj', $('#box-wgj'), '3d/180/bg/WGJ/WG00', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-wgj').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec3 
	// 切换
	$('.sec3 .option-item1').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec3 .content-item1').addClass('active').siblings().removeClass('active');
	});
	$('.sec3 .option-item2').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec3 .content-item2').addClass('active').siblings().removeClass('active');
		if(!$('.sec3 .box-wind3-2').hasClass('windAn3-2')) {
			$('.sec3 .box-wind3-2').addClass('windAn3-2');
		};
	});

	// wind effect
	function windAn3() {
		if($.visionAn($('.sec3 .box-wind3-1'), 0.7, 1)) {
			$('.sec3 .box-wind3-1').addClass('windAn3-1');
		};
	};
	windAn3();
	$(window).on('scroll', function() {
		windAn3();
	});

	// sec4
	// swiper
	var swiper4 = new Swiper('.sec4 .swiper4', {
		noSwiping: true,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 86, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton:' .sec4 .swiper-button-prev',
		nextButton:' .sec4 .swiper-button-next',
		pagination: '.sec4 .swiper-pagination',
		paginationType : 'progress'
	});
});