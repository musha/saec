jQuery(function($) {
	// 淡入动画
	$.fadeUpAn($('.sec2 .box-title2'));
	$.fadeUpAn($('.sec2 .box-text2-1'));
	$.fadeUpAn($('.sec2 .box-text2-2'));
    $.fadeUpAn($('.sec3 .box-title3'));
    $.fadeUpAn($('.sec3 .box-text3-1'));
	$.fadeUpAn($('.sec3 .box-text3-2'));
	$.fadeUpAn($('.sec4 .box-title4'));
	$.fadeUpAn($('.sec4 .box-text4-1'));
	$.fadeUpAn($('.sec4 .box-content4'));
	$.fadeUpAn($('.sec4 .box-text4-2'));
	$.fadeUpAn($('.sec4 .box-cards'));
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-text2-1'));
		$.fadeUpAn($('.sec2 .box-text2-2'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .box-text3-1'));
		$.fadeUpAn($('.sec3 .box-text3-2'));
        $.fadeUpAn($('.sec4 .box-title4'));
        $.fadeUpAn($('.sec4 .box-text4-1'));
        $.fadeUpAn($('.sec4 .box-content4'));
        $.fadeUpAn($('.sec4 .box-text4-2'));
        $.fadeUpAn($('.sec4 .box-cards'));
	});

	// sec1
	// download
	$('.box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
    });
	
	// sec2
	// text effect
	function textAn() {
		if($.visionAn($('.sec2 .text2-3'), 0.65, 1)) {
			$('.sec2 .text2-3-1').addClass('animated fadeInUp');
			setTimeout(function() {
				$('.sec2 .text2-3-2').addClass('animated fadeInUp');
			}, 500);
			setTimeout(function() {
				$('.sec2 .text2-3-3').addClass('animated fadeInUp');
			}, 1000);
			setTimeout(function() {
				$('.sec2 .text2-3-4').addClass('animated fadeInUp');
			}, 1500);
			setTimeout(function() {
				$('.sec2 .text2-3-5').addClass('animated fadeInUp');
			}, 2000);
			setTimeout(function() {
				$('.sec2 .text2-3-6').addClass('animated fadeInUp');
			}, 2500);
			setTimeout(function() {
				$('.sec2 .text2-4').addClass('animated fadeInUp');
			}, 3000);
		};
	};
	textAn()
	$(window).on('scroll', function() {
		textAn();
	});
    
	// sec4
	// content
	$('.sec4 .btn-sleepmode-1').on('click', function() {
		$('.bg01,.btn-sleepmode-1,.sleepmodeclose').removeClass('show').addClass('hide');
		$('.bg02,.btn-sleepmode-2,.sleepmodeopen').removeClass('hide').addClass('show');
	});
	$('.sec4 .btn-sleepmode-2').on('click', function() {
		$('.bg02,.btn-sleepmode-2,.sleepmodeopen').removeClass('show').addClass('hide');
		$('.bg01,.btn-sleepmode-1,.sleepmodeclose').removeClass('hide').addClass('show');
	});

	// 180 check
	$.prod180An('box-es', $('#box-es'), '3d/180/bg/ES/0_', 0, 36);
	
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-es').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});
});