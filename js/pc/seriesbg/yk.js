jQuery(function($) {
	// 淡入动画
	$.fadeUpAn($('.sec2 .box-title2'));
	$.fadeUpAn($('.sec2 .text2-1'));
	$.fadeUpAn($('.sec2 .text2-2'));
	$.fadeUpAn($('.sec2 .box-swiper'));
	$.fadeUpAn($('.sec3 .box-title3'));
	$.fadeUpAn($('.sec3 .box-text3-1'));
	$.fadeUpAn($('.sec3 .box-text3-2'));
	$.fadeUpAn($('.sec4 .box-title4'));
	$.fadeUpAn($('.sec4 .box-text4-1'));
	$.fadeUpAn($('.sec4 .box-text4-2'));
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .text2-1'));
		$.fadeUpAn($('.sec2 .text2-2'));
		$.fadeUpAn($('.sec2 .box-swiper'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .box-text3-1'));
		$.fadeUpAn($('.sec3 .box-text3-2'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .box-text4-1'));
		$.fadeUpAn($('.sec4 .box-text4-2'));
	});

	// sec1
	// download
	$('.box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// 180 check
	$.prod180An('box-yk', $('#box-yk'), '3d/180/bg/YK/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-yk').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec2 
	// swiper
	var swiper2 = new Swiper('.sec2 .swiper2', {
		// autoplay: 3000,
		autoplayDisableOnInteraction: false,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 84, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton:'.swiper-button-prev2',
		nextButton:'.swiper-button-next2',
		pagination: '.swiper-pagination2',
		paginationType : 'progress'
	});
});