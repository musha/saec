jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .text2-1'));
		$.fadeUpAn($('.sec2 .text2-2'));
		$.fadeUpAn($('.sec2 .qr2'));
		$.fadeUpAn($('.sec2 .box-swiper'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .text3-1'));
		$.fadeUpAn($('.sec3 .text3-2'));
		$.fadeUpAn($('.sec3 .box-swiper'));
	    $.fadeUpAn($('.sec4 .box-title4'));
	    $.fadeUpAn($('.sec4 .box-text'));
	    $.fadeUpAn($('.sec4 .box-swiper'));
	    $.fadeUpAn($('.sec5 .box-title5'));
	    $.fadeUpAn($('.sec5 .box-subtitle5'));
	    $.fadeUpAn($('.sec5 .text5-2-1'));
	    $.fadeUpAn($('.sec6 .box-title6'));
		$.fadeUpAn($('.sec6 .text6'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .text2-1'));
			$.fadeUpAn($('.sec2 .text2-2'));
			$.fadeUpAn($('.sec2 .qr2'));
			$.fadeUpAn($('.sec2 .box-swiper'));
	        $.fadeUpAn($('.sec3 .box-title3'));
	        $.fadeUpAn($('.sec3 .text3-1'));
	        $.fadeUpAn($('.sec3 .text3-2'));
	        $.fadeUpAn($('.sec3 .box-swiper'));
	        $.fadeUpAn($('.sec4 .box-title4'));
	        $.fadeUpAn($('.sec4 .box-text'));
	        $.fadeUpAn($('.sec4 .box-swiper'));
	        $.fadeUpAn($('.sec5 .box-title5'));
	        $.fadeUpAn($('.sec5 .box-subtitle5'));
	        $.fadeUpAn($('.sec5 .text5-2-1'));
	        $.fadeUpAn($('.sec6 .box-title6'));
			$.fadeUpAn($('.sec6 .text6'));
		});
	});

	// sec1
	// download
	$('.box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// 切换产品
	$('.sec1 .color-item' + $('.sec1 .box-prod img').data('index')).addClass('active');
	$('.sec1 .btn-360').attr('href', '3d/360/bg/ZT GZT/ZT/index.html');
	$('.sec1 .color-item').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec1 .box-prod img').data('index', $(this).data('color'));
		$('.sec1 .box-prod img').attr('src', 'img/pc/seriesbg/zt/prod1-' + $(this).data('color') + '.png');
		// 改变360链接
		switch(parseInt($(this).data('color'))) {
			case 1:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/ZT GZT/ZT/index.html');
				break;
			case 2:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/ZT GZT/GZT/index.html');
				break;
			default:
				//
		};
	});

	// 180 check
	$.prod180An('box-zt', $('#box-zt'), '3d/180/bg/ZT GZT/ZT改/0_', 0, 36);
	$.prod180An('box-zt', $('#box-gzt'), '3d/180/bg/ZT GZT/GZT/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(parseInt($('.sec1 .box-prod img').data('index'))) {
			case 1:
				$('.modal-zt').fadeIn(300);
				break;
			case 2:
				$('.modal-gzt').fadeIn(300);
				break;
			default:
				//
		};
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec2 
	// swiper
	var swiper2 = new Swiper('.sec2 .swiper2', {
		// autoplay: 3000,
		autoplayDisableOnInteraction: false,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 84, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton:'.swiper-button-prev2',
		nextButton:'.swiper-button-next2',
		pagination: '.swiper-pagination2',
		paginationType : 'progress'
    });
    
    // sec3 
	// swiper
	var swiper3 = new Swiper('.sec3 .swiper3', {
		// autoplay: 3000,
		autoplayDisableOnInteraction: false,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 84, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton:'.swiper-button-prev3',
		nextButton:'.swiper-button-next3',
		pagination: '.swiper-pagination3',
		paginationType : 'progress'
    });
    
    // sec4
	// swiper
	var swiper4 = new Swiper('.sec4 .swiper4', {
		// autoplay: 3000,
		autoplayDisableOnInteraction: false,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 84, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton:'.swiper-button-prev4',
		nextButton:'.swiper-button-next4',
		pagination: '.swiper-pagination4',
		paginationType : 'progress',
		onSlideChangeEnd: function(swiper) {
			$('.sec4 .box-text img').attr('src', 'img/pc/seriesbg/zt/text4-' + (swiper.realIndex + 1) + '.png');
		}   
    });
    
    // sec4
	// wind effect
	function windAn4() {
		if($.visionAn($('.sec4 .box-effect'), 0.7, 1)) {
			$('.sec4 .box-wind').addClass('windAn4');
		};
	};
	windAn4();
	$(window).on('scroll', function() {
		windAn4();
    });

    // sec5
	// text effect
	function textAn() {
		if($.visionAn($('.sec5 .text5-1'), 0.8, 1)) {
			$('.sec5 .text5-1-1').addClass('animated fadeInUp');
			setTimeout(function() {
				$('.sec5 .text5-1-2').addClass('animated fadeInUp');
			}, 500);
			setTimeout(function() {
				$('.sec5 .text5-1-3').addClass('animated fadeInUp');
			}, 1000);
			setTimeout(function() {
				$('.sec5 .text5-1-4').addClass('animated fadeInUp');
			}, 1500);
			setTimeout(function() {
				$('.sec5 .text5-1-5').addClass('animated fadeInUp');
			}, 2000);
			setTimeout(function() {
				$('.sec5 .text5-1-6').addClass('animated fadeInUp');
			}, 2500);
		};
	};
	textAn()
	$(window).on('scroll', function() {
		textAn();
	});
});