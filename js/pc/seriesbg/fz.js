jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .prod3'));
		$.fadeUpAn($('.sec3 .text3'));
		$.fadeUpAn($('.sec3 .card3-1'));
		$.fadeUpAn($('.sec3 .card3-2'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .text4-1'));
		$.fadeUpAn($('.sec4 .text4-2'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .text5-1'));
		$.fadeUpAn($('.sec5 .text5-2'));
		$.fadeUpAn($('.sec5 .box-swiper'));
		$.fadeUpAn($('.sec6 .box-title6'));
		$.fadeUpAn($('.sec6 .text6-1'));
		$.fadeUpAn($('.sec6 .text6-2'));
		$.fadeUpAn($('.sec6 .text6-3'));
		$.fadeUpAn($('.sec6 .swiper6'));
		$.fadeUpAn($('.sec6 .swiper6 .swiper-pagination'));
		$.fadeUpAn($('.sec7 .box-title7'));
		$.fadeUpAn($('.sec7 .text7'));
		$.fadeUpAn($('.sec8 .box-title8'));
		$.fadeUpAn($('.sec8 .text8-1'));
		$.fadeUpAn($('.sec8 .text8-2'));
		$.fadeUpAn($('.sec8 .text8-3'));
		$.fadeUpAn($('.sec9 .box-title9'));
		$.fadeUpAn($('.sec9 .text9-1'));
		$.fadeUpAn($('.sec9 .text9-2'));
		$.fadeUpAn($('.sec9 .text9-3'));
		$.fadeUpAn($('.sec9 .text9-4'));
		$.fadeUpAn($('.sec9 .text9-5'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .prod3'));
			$.fadeUpAn($('.sec3 .text3'));
			$.fadeUpAn($('.sec3 .card3-1'));
			$.fadeUpAn($('.sec3 .card3-2'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .text4-1'));
			$.fadeUpAn($('.sec4 .text4-2'));
			$.fadeUpAn($('.sec5 .box-title5'));
			$.fadeUpAn($('.sec5 .text5-1'));
			$.fadeUpAn($('.sec5 .text5-2'));
			$.fadeUpAn($('.sec5 .box-swiper'));
			$.fadeUpAn($('.sec6 .box-title6'));
			$.fadeUpAn($('.sec6 .text6-1'));
			$.fadeUpAn($('.sec6 .text6-2'));
			$.fadeUpAn($('.sec6 .text6-3'));
			$.fadeUpAn($('.sec6 .swiper6'));
			$.fadeUpAn($('.sec6 .swiper6 .swiper-pagination'));
			$.fadeUpAn($('.sec7 .box-title7'));
			$.fadeUpAn($('.sec7 .text7'));
			$.fadeUpAn($('.sec8 .box-title8'));
			$.fadeUpAn($('.sec8 .text8-1'));
			$.fadeUpAn($('.sec8 .text8-2'));
			$.fadeUpAn($('.sec8 .text8-3'));
			$.fadeUpAn($('.sec9 .box-title9'));
			$.fadeUpAn($('.sec9 .text9-1'));
			$.fadeUpAn($('.sec9 .text9-2'));
			$.fadeUpAn($('.sec9 .text9-3'));
			$.fadeUpAn($('.sec9 .text9-4'));
			$.fadeUpAn($('.sec9 .text9-5'));
		});
	}, 300);

	// sec1
	// download
	$('.sec1 .box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// 切换产品 初始值是1，第一个的颜色
	$('.sec1 .color-item' + $('.sec1 .box-prod img').data('index')).addClass('active');
	$('.sec1 .color-item').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec1 .box-prod img').data('index', $(this).data('color'));
		$('.sec1 .box-prod img').attr('src', 'img/pc/seriesbg/fz/prod1-' + $(this).data('color') + '.png');
	});
	// 180 查看
	$.prod180An('box-fz', $('#box-fz'), '3d/180/bg/FZ/0_', 0, 36);
	// 180 弹窗
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-fz').fadeIn(300);
	});
	// 关闭弹窗
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec2
	// player
	$('.sec2 .btn-player').on('click', function() {
		$('.modal-player').fadeIn(300);
		$('#playerfz')[0].play();
	});
	// close player
	$('.modal-player .btn-close').on('click', function() {
		$('.modal-player').fadeOut(300);
		$('#playerfz')[0].pause();
	});

	// sec5
	// swiper
	var swiper5 = new Swiper('.sec5 .swiper5', {
		noSwiping: true, // 禁止手滑
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 86, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton: '.sec5 .swiper-button-prev',
		nextButton: '.sec5 .swiper-button-next',
		pagination: '.sec5 .swiper-pagination',
		paginationType : 'progress'
	});

	// sec6
	var typeArr = ['制冷', '制热'];
	// swiper prod
	var swiper6 = new Swiper('.sec6 .swiper6', {
		noSwiping: true,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 64, /* 值越小两边的slide越靠近中间的slide */
            depth: 50,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		prevButton: '.sec6 .swiper-button-prev',
		nextButton: '.sec6 .swiper-button-next',
		pagination: '.sec6 .swiper-pagination',
		paginationClickable: true,
		paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><span>' + typeArr[index] + '</span></div>';
        },
		onSlideChangeEnd: function(swiper) {
			$('.sec6 .bg-item' + (swiper.realIndex + 1)).addClass('bgAn1').removeClass('bgAn2').siblings().removeClass('bgAn1').addClass('bgAn2');
			switch(swiper.realIndex) {
				case 0:
					$('.sec6 .box-title6').addClass('colorcold').removeClass('colorhot');
					$('.sec6 .box-text6-1').addClass('active');
					$('.sec6 .box-text6-2').removeClass('active');
					$('.sec6 .swiper6 .swiper-pagination').addClass('colorcold').removeClass('colorhot');
					break;
				case 1:
					$('.sec6 .box-title6').addClass('colorhot').removeClass('colorcold');
					$('.sec6 .box-text6-2').addClass('active');
					$('.sec6 .box-text6-1').removeClass('active');
					$('.sec6 .swiper6 .swiper-pagination').addClass('colorhot').removeClass('colorcold');
					break;
				default:
					//
			};
		}
	});
});