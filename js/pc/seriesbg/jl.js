// 要等DOM加载完成
jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .prod2'));
		$.fadeUpAn($('.sec2 .box-text'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .swiperprod'));
		$.fadeUpAn($('.sec4 .swiperprod .swiper-pagination'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .swiperprod'));
		$.fadeUpAn($('.sec5 .swiperprod .swiper-pagination'));
		$.fadeUpAn($('.sec6 .prod6-1'));
		$.fadeUpAn($('.sec6 .prod6-2'));
		$.fadeUpAn($('.sec6 .prod6-3'));
		$.fadeUpAn($('.sec6 .prod6-4'));
		$.fadeUpAn($('.sec6 .title6'));
		$.fadeUpAn($('.sec6 .box-title6'));
		$.fadeUpAn($('.sec6 .subtitle6'));
		$.fadeUpAn($('.sec7 .box-title7'));
		$.fadeUpAn($('.sec7 .text7-1'));
		$.fadeUpAn($('.sec7 .text7-2'));
		$.fadeUpAn($('.sec7 .box-swiper'));
		$.fadeUpAn($('.sec8 .box-title8'));
		$.fadeUpAn($('.sec8 .text8-1'));
		$.fadeUpAn($('.sec8 .text8-2'));
		$.fadeUpAn($('.sec8 .card8-1'));
		$.fadeUpAn($('.sec8 .card8-2'));
		$.fadeUpAn($('.sec9 .box-title9'));
		$.fadeUpAn($('.sec9 .prod9'));
		$.fadeUpAn($('.sec9 .text9-1'));
		$.fadeUpAn($('.sec9 .text9-2'));
		$.fadeUpAn($('.sec10 .box-title10'));
		$.fadeUpAn($('.sec10 .text10'));
		$.fadeUpAn($('.sec11 .box-title11'));
		$.fadeUpAn($('.sec11 .text11-1'));
		$.fadeUpAn($('.sec11 .text11-2'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .prod2'));
			$.fadeUpAn($('.sec2 .box-text'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .swiperprod'));
			$.fadeUpAn($('.sec4 .swiperprod .swiper-pagination'));
			$.fadeUpAn($('.sec5 .box-title5'));
			$.fadeUpAn($('.sec5 .swiperprod'));
			$.fadeUpAn($('.sec5 .swiperprod .swiper-pagination'));
			$.fadeUpAn($('.sec6 .prod6-1'));
			$.fadeUpAn($('.sec6 .prod6-2'));
			$.fadeUpAn($('.sec6 .prod6-3'));
			$.fadeUpAn($('.sec6 .prod6-4'));
			$.fadeUpAn($('.sec6 .title6'));
			$.fadeUpAn($('.sec6 .box-title6'));
			$.fadeUpAn($('.sec6 .subtitle6'));
			$.fadeUpAn($('.sec7 .box-title7'));
			$.fadeUpAn($('.sec7 .text7-1'));
			$.fadeUpAn($('.sec7 .text7-2'));
			$.fadeUpAn($('.sec7 .box-swiper'));
			$.fadeUpAn($('.sec8 .box-title8'));
			$.fadeUpAn($('.sec8 .text8-1'));
			$.fadeUpAn($('.sec8 .text8-2'));
			$.fadeUpAn($('.sec8 .card8-1'));
			$.fadeUpAn($('.sec8 .card8-2'));
			$.fadeUpAn($('.sec9 .box-title9'));
			$.fadeUpAn($('.sec9 .prod9'));
			$.fadeUpAn($('.sec9 .text9-1'));
			$.fadeUpAn($('.sec9 .text9-2'));
			$.fadeUpAn($('.sec10 .box-title10'));
			$.fadeUpAn($('.sec10 .text10'));
			$.fadeUpAn($('.sec11 .box-title11'));
			$.fadeUpAn($('.sec11 .text11-1'));
			$.fadeUpAn($('.sec11 .text11-2'));
		});
	}, 300);

	// sec1
	// download
	$('.box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// 切换产品 初始值是1
	$('.sec1 .color-item' + $('.sec1 .box-prod img').data('index')).addClass('active');
	$('.sec1 .btn-360').attr('href', '3d/360/bg/JL SJL GJL/SJL/index.html');
	$('.sec1 .color-item').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec1 .box-prod img').data('index', $(this).data('color'));
		$('.sec1 .box-prod img').attr('src', 'img/pc/seriesbg/jl/prod1-' + $(this).data('color') + '.png');
		// 改变360链接
		switch(parseInt($(this).data('color'))) {
			case 1:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/JL SJL GJL/JL/index.html');
				break;
			case 2:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/JL SJL GJL/SJL/index.html');
				break;
			case 3:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/JL SJL GJL/GJL/index.html');
				break;
			default:
				//
		};
	});
	// 180 check
	$.prod180An('box-jl', $('#box-jl'), '3d/180/bg/JL SJL GJL/JL/0_', 1, 37);
	$.prod180An('box-sjl', $('#box-sjl'), '3d/180/bg/JL SJL GJL/SJL/0_', 0, 36);
	$.prod180An('box-gjl', $('#box-gjl'), '3d/180/bg/JL SJL GJL/GJL/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(parseInt($('.sec1 .box-prod img').data('index'))) {
			case 1:
				$('.modal-jl').fadeIn(300);
				break;
			case 2:
				$('.modal-sjl').fadeIn(300);
				break;
			case 3:
				$('.modal-gjl').fadeIn(300);
				break;
			default:
				//
		};
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sect2 
	// 切换
	$('.sec2 .btn-check-plus').on('click', function() {
		$('.sec2 .text2-1').addClass('hide').siblings().removeClass('hide');
	});
	$('.sec2 .text2-2').on('click', function() {
		$(this).addClass('hide').siblings().removeClass('hide');
	});

	// sec3
	// player
	$('.sec3 .btn-player').on('click', function() {
		$('.modal-player').fadeIn(300);
		$('#playerjl')[0].play();
	});
	// close player
	$('.modal-player .btn-close').on('click', function() {
		$('.modal-player').fadeOut(300);
		$('#playerjl')[0].pause();
	});

	// sec7 
	// swiper
	var swiper7 = new Swiper('.sec7 .swiper7', {
		noSwiping: true,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 86, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton: '.sec7 .swiper-button-prev',
		nextButton: '.sec7 .swiper-button-next',
		pagination: '.sec7 .swiper-pagination',
		paginationType : 'progress'
	});

	// sec8
	// wind effect
	function windAn() {
		if($.visionAn($('.sec8 .box-effect'), 0.9, 1)) {
			$('.sec8 .box-wind').addClass('windAn');
		};
	};
	windAn();
	$(window).on('scroll', function() {
		windAn();
	});
});

// 要等图片加载完成
$(window).load(function() {
	// sec4
	// swiper bg
	var swiperbg4 = new Swiper('.sec4 .swiperbg', {
		noSwiping: true,
		effect: 'fade'
	});
	// swiper prod
	var swiperprod4 = new Swiper('.sec4 .swiperprod', {
		noSwiping: true,
		pagination: '.sec4 .swiper-paginationprod',
		paginationClickable: true,
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><img class="text4-' + (index + 1) + '-1 full-w" src="img/pc/seriesbg/jl/text4-' + (index + 1) + '-1.png"><img class="text4-' + (index + 1) + '-2 full-w" src="img/pc/seriesbg/jl/text4-' + (index + 1) + '-2.png"></div>';
        }
	});

	// sec5
	// swiper bg
	var swiperbg5 = new Swiper('.sec5 .swiperbg', {
		noSwiping: true,
		effect: 'fade'
	});
	var seriesArr = ['JL', 'SJL', 'GJL'];
	// swiper prod
	var swiperprod5 = new Swiper('.sec5 .swiperprod', {
		noSwiping: true,
		pagination: '.sec5 .swiper-paginationprod',
		paginationClickable: true,
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><div class="color color' + (index + 1) + '"></div><div class="series">' + seriesArr[index] + '</div><div class="panel">系列面板</div></div>';
        },
		onSlideChangeEnd: function(swiper) {
			$('.sec5 .title-item' + (swiper.realIndex + 1)).removeClass('hide').siblings().addClass('hide');
			$('.sec11 .bg11').attr('src', 'img/pc/seriesbg/jl/bg11-' + (swiper.realIndex + 1) + '.png');
			switch(swiper.realIndex) {
				case 0:
					$('.sec5 .swiperprod .swiper-pagination').addClass('color1').removeClass('color2').removeClass('color3');
					break;
				case 1:
				$('.sec5 .swiperprod .swiper-pagination').addClass('color2').removeClass('color1').removeClass('color3');
					break;
				case 2:
				$('.sec5 .swiperprod .swiper-pagination').addClass('color3').removeClass('color1').removeClass('color2');
					break;
				default:
					//
			};
		}
	});

	// swiper control
	swiperprod4.params.control = [swiperbg4, swiperprod5];
	swiperprod5.params.control = [swiperbg5, swiperprod4];
});