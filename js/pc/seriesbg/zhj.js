// 要等DOM加载完成
jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .prod2'));
		$.fadeUpAn($('.sec2 .text2-1'));
		$.fadeUpAn($('.sec2 .text2-2'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .prod3'));
		$.fadeUpAn($('.sec3 .text3-1'));
		$.fadeUpAn($('.sec3 .text3-2'));
		$.fadeUpAn($('.sec3 .text3-3'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .text4-1'));
		$.fadeUpAn($('.sec4 .text4-2'));
		$.fadeUpAn($('.sec4 .box-swiper'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .text5-1'));
		$.fadeUpAn($('.sec5 .text5-2'));
		$.fadeUpAn($('.sec5 .card5-1'));
		$.fadeUpAn($('.sec5 .card5-2'));
		$.fadeUpAn($('.sec6 .box-title6'));
		$.fadeUpAn($('.sec6 .prod6'));
		$.fadeUpAn($('.sec6 .text6-1-1'));
		$.fadeUpAn($('.sec6 .text6-1-2'));
		$.fadeUpAn($('.sec6 .text6-2-1'));
		$.fadeUpAn($('.sec6 .text6-2-2'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .prod2'));
			$.fadeUpAn($('.sec2 .text2-1'));
			$.fadeUpAn($('.sec2 .text2-2'));
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .prod3'));
			$.fadeUpAn($('.sec3 .text3-1'));
			$.fadeUpAn($('.sec3 .text3-2'));
			$.fadeUpAn($('.sec3 .text3-3'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .text4-1'));
			$.fadeUpAn($('.sec4 .text4-2'));
			$.fadeUpAn($('.sec4 .box-swiper'));
			$.fadeUpAn($('.sec5 .box-title5'));
			$.fadeUpAn($('.sec5 .text5-1'));
			$.fadeUpAn($('.sec5 .text5-2'));
			$.fadeUpAn($('.sec5 .card5-1'));
			$.fadeUpAn($('.sec5 .card5-2'));
			$.fadeUpAn($('.sec6 .box-title6'));
			$.fadeUpAn($('.sec6 .prod6'));
			$.fadeUpAn($('.sec6 .text6-1-1'));
			$.fadeUpAn($('.sec6 .text6-1-2'));
			$.fadeUpAn($('.sec6 .text6-2-1'));
			$.fadeUpAn($('.sec6 .text6-2-2'));
		});
	}, 300);

	// sec1
	// download
	$('.box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// 切换产品 初始值是1，第一个的颜色
	$('.sec1 .color-item' + $('.sec1 .box-prod img').data('index')).addClass('active');
	$('.sec1 .btn-360').attr('href', '3d/360/bg/ZHJ PZHJ AHJ PAHJ/ZHJ/index.html');
	$('.sec1 .color-item').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec1 .box-prod img').data('index', $(this).data('color'));
		$('.sec1 .box-prod img').attr('src', 'img/pc/seriesbg/zhj/prod1-' + $(this).data('color') + '.png');
		// 改变360链接
		switch(parseInt($(this).data('color'))) {
			case 1:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/ZHJ PZHJ AHJ PAHJ/ZHJ/index.html');
				break;
			case 2:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/ZHJ PZHJ AHJ PAHJ/PZHJ/index.html');
				break;
			default:
				//
		};
	});
	// 180 check
	$.prod180An('box-zhj', $('#box-zhj'), '3d/180/bg/ZHJ PZHJ AHJ PAHJ/ZHJ AHJ/ZHJ00', 0, 36);
	$.prod180An('box-pzhj', $('#box-pzhj'), '3d/180/bg/ZHJ PZHJ AHJ PAHJ/PZHJ PAHJ/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(parseInt($('.sec1 .box-prod img').data('index'))) {
			case 1:
				$('.modal-zhj').fadeIn(300);
				break;
			case 2:
				$('.modal-pzhj').fadeIn(300);
				break;
			default:
				//
		};
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec4
	// swiper
	var swiper4 = new Swiper('.sec4 .swiper4', {
		noSwiping: true, // 禁止手滑
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 86, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton: '.sec4 .swiper-button-prev',
		nextButton: '.sec4 .swiper-button-next',
		pagination: '.sec4 .swiper-pagination',
		paginationType : 'progress'
	});
});
// 要等图片加载完成
$(window).load(function() {
	// sec5
	// swiper
	var swiper5 = new Swiper('.sec5 .swiper5', {
		noSwiping: true, // 禁止手滑
		loop: true,
		effect: 'fade',
		prevButton: '.sec5 .swiper-button-prev',
		nextButton: '.sec5 .swiper-button-next',
		pagination: '.swiper-pagination',
		paginationClickable: true
	});
});