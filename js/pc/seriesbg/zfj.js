jQuery(function($) {
	// 淡入动画
    $.fadeUpAn($('.sec2 .box-title2'));
	$.fadeUpAn($('.sec2 .box-text2-1'));
	$.fadeUpAn($('.sec2 .box-text2-2'));
	$.fadeUpAn($('.sec3 .box-title3'));
	$.fadeUpAn($('.sec3 .text3-1'));
	$.fadeUpAn($('.sec3 .text3-2'));
	$.fadeUpAn($('.sec3 .box-swiper'));
    $.fadeUpAn($('.sec4 .box-title4'));
    $.fadeUpAn($('.sec4 .box-prod'));
    $.fadeUpAn($('.sec4 .box-text4-1'));
    $.fadeUpAn($('.sec4 .box-text4-2'));
    $.fadeUpAn($('.sec4 .box-card'));
	// scroll
	$(window).on('scroll', function() {
        $.fadeUpAn($('.sec2 .box-title2'));
        $.fadeUpAn($('.sec2 .box-text2-1'));
        $.fadeUpAn($('.sec2 .box-text2-2'));
        $.fadeUpAn($('.sec3 .box-title3'));
        $.fadeUpAn($('.sec3 .text3-1'));
        $.fadeUpAn($('.sec3 .text3-2'));
        $.fadeUpAn($('.sec3 .box-swiper'));
        $.fadeUpAn($('.sec4 .box-title4'));
        $.fadeUpAn($('.sec4 .box-prod'));
		$.fadeUpAn($('.sec4 .box-text4-1'));
		$.fadeUpAn($('.sec4 .box-text4-2'));
		$.fadeUpAn($('.sec4 .box-card'));
	});

	// sec1
	// download
	$('.box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
    });
    
    // 切换产品
	$('.sec1 .color-item' + $('.sec1 .box-prod img').data('index')).addClass('active');
	$('.sec1 .btn-360').attr('href', '3d/360/bg/ZFJ PZFJ/ZFJ/index.html');
	$('.sec1 .color-item').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec1 .box-prod img').data('index', $(this).data('color'));
		$('.sec1 .box-prod img').attr('src', 'img/pc/seriesbg/zfj/prod1-' + $(this).data('color') + '.png');
		// 改变360链接
		switch(parseInt($(this).data('color'))) {
			case 1:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/ZFJ PZFJ/ZFJ/index.html');
				break;
			case 2:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/ZFJ PZFJ/PZFJ/index.html');
				break;
			default:
				//
		};
	});

	// 180 check
	$.prod180An('box-zfj', $('#box-zfj'), '3d/180/bg/ZFJ PZFJ/ZFJ/ZFJ00', 0, 36);
	$.prod180An('box-pzfj', $('#box-pzfj'), '3d/180/bg/ZFJ PZFJ/PZFJ/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(parseInt($('.sec1 .box-prod img').data('index'))) {
			case 1:
				$('.modal-zfj').fadeIn(300);
				break;
			case 2:
				$('.modal-pzfj').fadeIn(300);
				break;
			default:
				//
		};
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
    });
    
    // sec2
	// swiper
	// var swiperbg = new Swiper('.sec2 .swiperbg', {
	// 	loop: true,
	// 	effect: 'fade'
	// });
	// var swiperimg = new Swiper('.sec2 .swiperimg', {
	// 	noSwiping: true,
	// 	loop: true,
    //     prevButton:'.swiper-button-prev2',
	// 	nextButton:'.swiper-button-next2',
	// 	pagination: '.swiper-pagination2',
    //     onSlideChangeEnd: function(swiper) {
	// 		$('.sec2 .text2-1').attr('src', 'img/pc/seriesbg/zfj/text2-1-' + (swiper.realIndex + 1) + '.png');
    //         $('.sec2 .text2-2').attr('src', 'img/pc/seriesbg/zfj/text2-2-' + (swiper.realIndex + 1) + '.png');
    //         switch(parseInt(swiper.realIndex)) {
    //             case 0:
    //                 $('.sec2 .box-title2').css('color', '#4f5e65');
    //                 $('.sec2 .swiper-pagination-bullet-active').css('background', '#5f6d73').siblings().css('background', '#FFF');
    //                 break;
    //             case 1:
    //                 $('.sec2 .box-title2').css('color', '#563c2a');
    //                 $('.sec2 .swiper-pagination-bullet-active').css('background', '#563c2a').siblings().css('background', '#FFF');
    //                 break;
    //             default:
    //                 //
    //         };
	// 	}   
    // });
    // // swiper control
	// swiperimg.params.control = [swiperbg];

	// sec3 
	// swiper
	var swiper3 = new Swiper('.sec3 .swiper3', {
		// autoplay: 3000,
		autoplayDisableOnInteraction: false,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 84, /* 值越小两边的slide越靠近中间的slide */
            depth: 37,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton:'.swiper-button-prev3',
		nextButton:'.swiper-button-next3',
		pagination: '.swiper-pagination3',
		paginationType : 'progress'
	});
});