jQuery(function($) {
    // 淡入动画
    // setTimeout(function() {
    //     $.fadeUpAn($('.sec1 aside'));
    //     $.fadeUpAn($('.sec1 .content1'));
    //     // scroll
    //     $(window).on('scroll', function() {
    //         $.fadeUpAn($('.sec1 aside'));
    //         $.fadeUpAn($('.sec1 .content1'));
    //     });
    // }, 300);

    // 筛选条件
    var screenAside = {
        func1: 0,
        func2: 0,
        func3: 0,
        cons1: 0,
        cons2: 0,
        cons3: 0,
        feat1: 0,
        feat2: 0,
        areatype1: 0,
        areatype2: 0,
        area1: 0,
        area2: 0,
        area3: 0,
        area4: 0,
        area5: 0,
        area6: 0
    };

    // sec1
    // 获取产品分页
    var canGo = 1; // 默认可以跳转
    function getProd(func, cons, feat, areatype, area, pagenum) {
        if(func == null || func == undefined) {
            func = '';
        };
        if(cons == null || cons == undefined) {
            cons = '';
        };
        if(feat == null || feat == undefined) {
            feat = '';
        };
        if(areatype == null || areatype == undefined) {
            areatype = 0;
        };
        if(area == null || area == undefined) {
            area = '';
        };
        if(pagenum == null || pagenum == undefined) {
            pagenum = 1;
        };
        $('.sec1 .pagin').pagination({
            beforePaging: function() {
                // 正在调接口，不能重复调取
                $('.sec1 .modal-loading').fadeIn(300);
                $('.sec1 aside input').prop('disabled', true);
                $('.sec1 .btn-reset').prop('disabled', true);
            },
            dataSource: 'http://saec.gypserver.com/service/api/Home/GetChannelProducts',
            ajax: {
                type: 'get',
                // contentType: 'application/json',
                data: {
                    cnl: 4,
                    func: func,
                    consume: cons,
                    feature: feat,
                    areaType: areatype,
                    area: area,
                    category: '家用空调,柜式空调',
                    pType: 1,
                    page: pagenum,
                    pageSize: 12
                },
                // async: false,
                // cache: false,
                complete: function(data) {
                    // 调取完毕
                    $('.sec1 .modal-loading').fadeOut(300);
                    $('.sec1 aside input').prop('disabled', false);
                    $('.sec1 .btn-reset').prop('disabled', false);
                    // hover
                    $('.sec1 .prod-item').hover(function() {
                        $(this).addClass('shadow');
                    }, function() {
                        $(this).removeClass('shadow');
                    });
                }
            },
            // 参数别名
            alias: {
                pageNumber: 'page'
            },
            locator: 'data.data',
            pageSize: 12,
            prevText: '上一页',
            nextText: '下一页',
            // hideWhenLessThanOnePage: true,
            totalNumberLocator: function(res) {
                return res.data.total;
            },
            callback: function (data, pagination) {
                $('.sec1 .list-prod').empty();
                $('.sec1 .tip-error').addClass('hide');
                var len = data.length;
                if(len > 0) {
                    // 有数据
                    $('.sec1 .tip-no').addClass('hide');
                    for(var i = 0; i < len; i++) {
                        var url = data[i].linkUrl,
                            target = '_self';
                        if(url.length == 0) {
                            // 没有链接
                            url = '#';
                        } else {
                            // 有链接且为内部链接
                            if(url.indexOf('http') < 0) {
                                url = 'p.' + url + '.html';
                            } else {
                                // 外部链接
                                target = '_blank';
                            };
                        };
                        var $a = $('<a href="' + url + '" target="' + target + '"><li class="prod-item rel"><div class="prod-view center"><img class="full-w" src="http://saec.gypserver.com/' + data[i].imageUrl.substring(1) + '"></div><div class="prod-name text-center">' + data[i].title + '</div></li></a>');
                        $('.sec1 .list-prod').append($a);
                    };
                } else {
                    // 无数据
                    $('.sec1 .tip-no').removeClass('hide');
                };
            },
            formatAjaxError: function(jqXHR, textStatus, errorThrown) {
                // 错误提示
                $('.sec1 .tip-error').removeClass('hide');
            },
            afterPageOnClick: function() {
                canGo = 0;
            },
            afterPreviousOnClick: function() {
                canGo = 0;
            },
            afterNextOnClick: function() {
                canGo = 0;
            },
            afterPaging: function(page) {
                if(canGo == 0) {
                    // 不可以跳转
                    sessionStorage.setItem('currentPage', JSON.stringify(page));
                } else {
                    // 可以跳转
                    if(JSON.parse(sessionStorage.getItem('currentPage')) != null || JSON.parse(sessionStorage.getItem('currentPage')) != undefined) {
                        if(page != JSON.parse(sessionStorage.getItem('currentPage'))) {
                            $('.sec1 .pagin').pagination(JSON.parse(sessionStorage.getItem('currentPage')));
                        };
                    };
                };
            }
        });
    };

    // 是否有存储筛选条件
    if(JSON.parse(sessionStorage.getItem('screenSelect')) != null || JSON.parse(sessionStorage.getItem('screenSelect')) != undefined) {
        // console.log('来自智能选型');
        sessionStorage.removeItem('screenAsideGsModel');
        sessionStorage.removeItem('currentPage');
        var screenSelect = JSON.parse(sessionStorage.getItem('screenSelect'));
        sessionStorage.setItem('screenSelectNew', JSON.stringify(screenSelect));
        var screenSelectNew = JSON.parse(sessionStorage.getItem('screenSelectNew'));
        sessionStorage.removeItem('screenSelect');
        setTimeout(function() {
            var l = $('aside input').length;
            for(var i = 0; i < l; i++) {
                if($('aside input')[i].value == screenSelect['areatype'] || $('aside input')[i].value == screenSelect['area']) {
                    $($('aside input')[i]).prop('checked', true);
                    $($('aside input')[i]).parents('.box-select').removeClass('hide');
                    $($('aside input')[i]).parents('.box-select').siblings('.title').find('.divider2').addClass('hide');
                    screenAside[$($('aside input')[i]).attr('id')] = 1;
                    sessionStorage.setItem('screenAsideGsModel', JSON.stringify(screenAside));
                };
            };
            getProd('', '', '', screenSelectNew['areatype'], screenSelectNew['area'], 1);
        }, 300);
    } else {
        // console.log('刷新');
        if(JSON.parse(sessionStorage.getItem('screenAsideGsModel')) != null || JSON.parse(sessionStorage.getItem('screenAsideGsModel')) != undefined) {
            // 已有筛选
            screenAside = JSON.parse(sessionStorage.getItem('screenAsideGsModel'));
            $.each(screenAside, function(key ,value) {
                if(value == 0) {
                    $('#' + key).prop('checked', false);
                } else {
                    $('#' + key).prop('checked', true);
                    $('#' + key).parents('.box-select').removeClass('hide');
                    $('#' + key).parents('.box-select').siblings('.title').find('.divider2').addClass('hide');
                };
            });
            setTimeout(function() {
                var func = screenFun(func, $('.sec1 .item-function')),
                cons = screenFun(cons, $('.sec1 .item-consume')),
                feat = screenFun(feat, $('.sec1 .item-feature')),
                area = screenFun(area, $('.sec1 .item-area')),
                areatype = screenFun(areatype, $('.sec1 .item-areatype'));
                getProd(func, cons, feat, areatype, area, 1);
            }, 300);
        } else {
            // 没有筛选
            sessionStorage.removeItem('currentPage');
            getProd('', '', '', 0, 0, 1);
        };
    };

    // 筛选效果
    $('aside .title').on('click', function() {
        var that = $(this);
        $(this).find('.divider2').toggleClass('hide');
        $(this).next().stop(true, true).slideToggle();
    });

    // 筛选函数
    function screenFun(name, el) {
        if(el.find(':input:checked').length > 0) {
            // 选了
            var arr = [];
            var len = el.find(':input:checked').length;
            for(var i = 0; i < len; i++) {
                arr.push($(el.find(':input:checked')[i]).val());
            };
            name = arr.join(',');
        } else {
            // 没选
            name = '';
        };
        return name;
    };

    //
    // 筛选提交
    $('aside label').on('click', function() {
        var that = $(this);
        var func,
            cons,
            feat,
            areatype,
            area;
        setTimeout(function() {
            if(that.siblings('input').is(':checked')) {
                screenAside[that.siblings('input').attr('id')] = 1;
            } else {
                screenAside[that.siblings('input').attr('id')] = 0;
            };
            sessionStorage.setItem('screenAsideGsModel', JSON.stringify(screenAside));
            func = screenFun(func, $('.sec1 .item-function'));
            cons = screenFun(cons, $('.sec1 .item-consume'));
            feat = screenFun(feat, $('.sec1 .item-feature'));
            areatype = screenFun(areatype, $('.sec1 .item-areatype'));
            area = screenFun(area, $('.sec1 .item-area'));
            sessionStorage.removeItem('currentPage');
            getProd(func, cons, feat, areatype, area, 1);
        }, 300);
    });

    // 重置
    $('aside .btn-reset').on('click', function() {
        $('aside').find('input:checked').prop('checked', false);
        setTimeout(function() {
            $.each(screenAside, function(key ,value) {
                if(screenAside[key] == 1) {
                    screenAside[key] = 0;
                };
            });
            sessionStorage.setItem('screenAsideGsModel', JSON.stringify(screenAside));
            sessionStorage.removeItem('currentPage');
            getProd('', '', '', 0, 0, 1);
        }, 300);
    });
});