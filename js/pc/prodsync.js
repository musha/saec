jQuery(function($) {
    // 淡入动画
    setTimeout(function() {
        $.fadeUpAn($('.sec1 .content1'));
        // scroll
        $(window).on('scroll', function() {
            $.fadeUpAn($('.sec1 .content1'));
        });
    }, 300);

    // sec1
    // 获取产品
    var canProd = 1; // 是否可以调接口 默认可以
    function getProd() {
        if(canProd == 1) {
            // 正在调接口，不能重复调取
            $('.sec1 .modal-loading').fadeIn(300);
            canProd = 0;
            $.ajax({
                type: 'get',
                url: 'http://saec.gypserver.com/service/api/Home/GetChannelProducts',
                dataType: 'json',
                // contentType: 'application/json',
                data: {
                    cnl: 4,
                    category: '商用空调,内藏式风管机',
                    pType: 0
                },
                // async: false,
                cache: false,
                success: function(data) {
                    // console.log(data);
                    if(data.success) {
                        $('.sec1 .list-prod').empty();
                        $('.sec1 .tip-error').addClass('hide');
                        var len = data.data.data.length;
                        if(len > 0) {
                            // 有数据
                            $('.sec1 .tip-no').addClass('hide');
                            for(var i = 0; i < len; i++) {
                                var url = data.data.data[i].linkUrl,
                                    target = '_self';
                                if(url.length == 0) {
                                    // 没有链接
                                    url = '#';
                                } else {
                                    // 有链接且为内部链接
                                    if(url.indexOf('http') < 0) {
                                        url = 'p.' + url + '.html';
                                    } else {
                                        // 外部链接
                                        target = '_blank';
                                    };
                                };
                                var $a = $('<a href="' + url + '" target="' + target + '"><li class="prod-item rel"><div class="prod-view center"><img class="full-w" src="http://saec.gypserver.com/' + data.data.data[i].imageUrl.substring(1) + '"></div><div class="prod-name text-center">' + data.data.data[i].title + '</div></li></a>');
                                $('.sec1 .list-prod').append($a);
                            };
                        } else {
                            // 无数据
                            $('.sec1 .tip-no').removeClass('hide');
                        };
                    };
                },
                error: function() {
                    // 错误提示
                    $('.sec1 .tip-error').removeClass('hide');
                },
                complete: function(data) {
                    // 调取完毕
                    $('.sec1 .modal-loading').fadeOut(300);
                    canProd = 1;
                    // hover
                    $('.sec1 .prod-item').hover(function() {
                        $(this).addClass('shadow');
                    }, function() {
                        $(this).removeClass('shadow');
                    });
                }
            });
        }
    };
    getProd();
});