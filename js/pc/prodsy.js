jQuery(function($) {
    // 淡入动画
    setTimeout(function() {
        $.fadeUpAn($('.sec1 .box-title1'));
        $.fadeUpAn($('.sec1 .content1'));
        // scroll
        $(window).on('scroll', function() {
            $.fadeUpAn($('.sec1 .box-title1'));
            $.fadeUpAn($('.sec1 .content1'));
        });
    }, 300);

    // sec1 
    // hover
    $('.sec1 .content1 div.rel').hover(function() {
        $(this).addClass('shadow');
    }, function() {
        $(this).removeClass('shadow');
    });
});