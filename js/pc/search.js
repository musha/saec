jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		//
		// scroll
		$(window).on('scroll', function() {
			//
		});
	}, 300);

    // 导航搜索
    if(JSON.parse(sessionStorage.getItem('keyword')) != null || JSON.parse(sessionStorage.getItem('keyword')) != undefined) {
        $('.sec0 input').val(JSON.parse(sessionStorage.getItem('keyword')));
        search(JSON.parse(sessionStorage.getItem('keyword')));
        sessionStorage.removeItem('keyword');
    };

	// sec0
	$('.sec0 .btn-search').on('click', function() {
		if($('.sec0 input').val() == '') {
			alert('请输入');
		} else {
			search($('.sec0 input').val());
		};
	});

	// 调取搜索
    function search(keyword) {
    	$('.modal-loading').fadeIn(300);
        $.ajax({
            type: 'get',
            url: 'http://saec.gypserver.com/service/api/Home/Search',
            dataType: 'json',
            // contentType: 'application/json',
            data: {
                cnl: 0,
                page: 0,
                content: keyword
            },
            // async: false,
            cache: false,
            success: function(data) {
                // console.log(data);
                if(data.success) {
                	$('.sec1 .content-result').empty();
                    $('.sec1 .tip-error').addClass('hide');
                 	var len = data.data.data.length;
                 	if(len > 0) {
                 		$('.sec1 .tip-no').addClass('hide');
                 		for(var i = 0; i < len; i++) {
                            var url = data.data.data[i].linkUrl,
                                target = '_self';
                            if(url.length == 0) {
                                // 没有链接
                                url = '#';
                            } else {
                                // 有链接且为内部链接
                                if(url.indexOf('http') < 0) {
                                    url = 'p.' + url + '.html';
                                } else {
                                    // 外部链接
                                    target = '_blank';
                                }
                            };
                 			var $div = $('<div class="res-item bg-white"><div class="res-title"><a href="' + url + '" target="' + target + '"><span class="title">' + data.data.data[i].title + '</span></a></div></div>');
                            $('.sec1 .content-result').append($div);
                 		}
                 	} else  {
                 		$('.sec1 .tip-no').removeClass('hide');
                 	};
                };
            },
            error: function() {
                // 错误提示
                $('.sec1 .tip-error').removeClass('hide');
            },
            complete: function(data) {
                // 调取完毕
                $('.modal-loading').fadeOut(300);
                // 内容折叠
                // $('.sec1 .res-title').on('click', function() {
                //     $(this).next().slideToggle();
                // });
            }
        });
    };
});