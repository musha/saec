jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .content1'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-swiper'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec1 .box-title1'));
			$.fadeUpAn($('.sec1 .content1'));
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .box-swiper'));
		});
	}, 300);
	
	// sec2
	// swiper
	var swiper2 = new Swiper('.sec2 .swiper2', {
		slidesPerView: 3,
		spaceBetween : 56,
		mousewheelControl: true,
    	mousewheelReleaseOnEdges: true,
    	prevButton: '.sec2 .swiper-button-prev',
		nextButton: '.sec2 .swiper-button-next',
		pagination: '.sec2 .swiper-pagination',
		paginationType : 'progress'
	});
});