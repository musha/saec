jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec0 .box-title0'));
		$.fadeUpAn($('.sec0 .box-player'));
		$.fadeUpAn($('.sec0 .conts'));
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .year1994'));
		$.fadeUpAn($('.sec1 .year2005'));
		$.fadeUpAn($('.sec1 .year2014'));
		$.fadeUpAn($('.sec1 .year2016'));
		$.fadeUpAn($('.sec1 .year2019'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .content2'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec0 .box-title0'));
			$.fadeUpAn($('.sec0 .box-player'));
		$.fadeUpAn($('.sec0 .conts'));
			$.fadeUpAn($('.sec1 .box-title1'));
			$.fadeUpAn($('.sec1 .year1994'));
			$.fadeUpAn($('.sec1 .year2005'));
			$.fadeUpAn($('.sec1 .year2014'));
			$.fadeUpAn($('.sec1 .year2016'));
			$.fadeUpAn($('.sec1 .year2019'));
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .content2'));
		});
	}, 300);

	// sec0 
	// player
	var playerjl = $('#playerjl');
	$('.sec0 .btn-player').on('click', function() {
		$('.modal-jl').fadeIn(300);
		playerjl[0].play();
	});
	// close player
	$('.modal .btn-close').on('click', function() {
		if($(this).parent().next('video')[0].paused) {
			//
		} else {
			$(this).parent().next('video')[0].pause();
		};
		$('.modal').fadeOut(300);
	});

	// 全域慧眼
	$('.sec2 .btn-check').on('click', function() {
		sessionStorage.setItem('selectFun', JSON.stringify($(this).data('func')));
	});

	// hover
	$('.btn-check').hover(function() {
		$(this).attr('src', 'img/pc/moveeye/btn-check-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/moveeye/btn-check.png');
	});
});