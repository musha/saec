// 要等图片加载完成
$(window).load(function() {
	// sec1
	// swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
        noSwiping: true,
		loop: true,
		effect: 'fade',
		prevButton: '.sec1 .swiper-button-prev',
		nextButton: '.sec1 .swiper-button-next',
		pagination: '.sec1 .swiper-pagination'
	});

    // 使pagination比例不变
    function pagiFun() {
        var setpagi = setInterval(function() {
            if($('.sec1 .swiper-pagination .swiper-pagination-bullet').height() != $('.sec1 .swiper-pagination .swiper-pagination-bullet').width()) {
                $('.sec1 .swiper-pagination .swiper-pagination-bullet').css({
                    height: $('.sec1 .swiper-pagination .swiper-pagination-bullet').width() + 'px'
                });
            } else {
                clearInterval(setpagi);
            };
        }, 300);
    };
    pagiFun()
    $(window).on('resize', function() {
        pagiFun();
    });
});
// 要等DOM加载完成
jQuery(function($) {
    // 淡入动画
    setTimeout(function() {
        $.fadeUpAn($('.sec1 .box-title'));
        $.fadeUpAn($('.sec1 .swiper-pagination'));
        $.fadeUpAn($('.sec2 .box-title2'));
        $.fadeUpAn($('.sec2 .box-year'));
        $.fadeUpAn($('.sec3 .box-title3'));
        // scroll
        $(window).on('scroll', function() {
            $.fadeUpAn($('.sec1 .box-title'));
            $.fadeUpAn($('.sec1 .swiper-pagination'));
            $.fadeUpAn($('.sec2 .box-title2'));
            $.fadeUpAn($('.sec2 .box-year'));
            $.fadeUpAn($('.sec3 .box-title3'));
        });
    }, 300);

	// sec2
    // 展开
    function slideRight(el) {
        el.find('.divider2').fadeOut(200);
        el.prev().stop(true, true).animate({width: '45.6rem'}, 600);
        el.prev().find('.year span').stop(true, true).fadeIn(200);
        el.prev().find('.year-event').stop(true, true).animate({opacity: 1}, 600);
    };
    // 收起
    function slideLeft(el) {
        el.find('.divider2').fadeIn(200);
        el.prev().stop(true, true).animate({width: '5rem'}, 600);
        el.prev().find('.year span').stop(true, true).fadeOut(200);
        el.prev().find('.year-event').stop(true, true).animate({opacity: 0}, 600);
    };

	// 调接口获取文本
	$.ajax({
        type: 'get',
        url: 'http://saec.gypserver.com/service/api/Home/GetChannelContents',
        dataType: 'json',
        // contentType: 'application/json',
        data: {
            cnl: 8
        },
        // async: false,
        cache: false,
        success: function(data) {
            // console.log(data);
            if(data.success) {
                $('.sec2 .box-year').empty();
                $('.sec2 .tip-error').addClass('hide');
                var len = data.data.data.length;
                if(len > 0) {
                    // 有数据
                    $('.sec2 .tip-no').addClass('hide');
                    // 加载年份事件
                    for(var i = 0; i < len; i++) {
                    	var $li = $('<li class="year-item full-h rel"><div class="year-content full-h rel"><div class="year">' + data.data.data[i].title + ' 年</div><div class="year-event abs">' + data.data.data[i].content + '</div></div><div class="btn-slide abs pointer"><div class="divider divider1"></div><div class="divider divider2 abs"></div></div></li>');
                    	$('.sec2 .box-year').append($li);
                    };
                } else {
                    // 无数据
                    $('.sec2 .tip-no').removeClass('hide');
                };
            };
        },
        error: function() {
            // 错误提示
            $('.sec2 .tip-error').removeClass('hide');
        },
        complete: function(data) {
            // 调取完毕
            $('.sec2 .modal-loading').fadeOut(300);
            $('.sec2 .btn-slide').on('click', function() {
                if($(this).prev().width() > 200) {
                    // 收起
                    slideLeft($(this));
                } else {
                    // 展开
                    slideRight($(this));
                    $(this).parents('.year-item').siblings().each(function() {
                        slideLeft($(this).find('.btn-slide'));
                    });
                };
            });
        }
    });
});