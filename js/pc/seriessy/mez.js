jQuery(function($) {
	// sec1
	// download
	$('.box-download0').on('click', function() {
		$('.modal-download0').fadeIn(300);
	});
	$('.modal-download0 .btn-close').on('click', function() {
		$('.modal-download0').fadeOut(300);
	});
	
	$('.box-download1').on('click', function() {
		$('.modal-download1').fadeIn(300);
	});
	$('.cont3_hide .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});
	$('.box-download2').on('click', function() {
		$('.modal-download2').fadeIn(300);
	});
	$('.cont3_hide .btn-close2').on('click', function() {
		$('.modal').fadeOut(300);
	});
	

	// // 淡入动画
	$.fadeUpAn($('.sec2 .cont2_dh1'));
	$.fadeUpAn($('.sec2 .cont2_p'));
	$.fadeUpAn($('.sec2 .cont2_img'));
	$.fadeUpAn($('.sec3 .cont3_dh1'));
	$.fadeUpAn($('.sec3 .cont3_p'));
	$.fadeUpAn($('.sec3 .cont3_img'));
	$.fadeUpAn($('.sec3 .gd_ckgd'));
	$.fadeUpAn($('.sec4 .cont4_dh1'));
	$.fadeUpAn($('.sec4 .cont4_p'));
	$.fadeUpAn($('.sec4 .gd_ckgd'));
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .cont2_dh1'));
		$.fadeUpAn($('.sec2 .cont2_img'));
		$.fadeUpAn($('.sec2 .cont2_p'));
		$.fadeUpAn($('.sec3 .cont3_dh1'));
		$.fadeUpAn($('.sec3 .cont3_p'));
		$.fadeUpAn($('.sec3 .cont3_img'));
		$.fadeUpAn($('.sec3 .gd_ckgd'));
		$.fadeUpAn($('.sec4 .cont4_dh1'));
		$.fadeUpAn($('.sec4 .cont4_p'));
		$.fadeUpAn($('.sec4 .gd_ckgd'));
	});
});