jQuery(function($) {
	// sec1
	// download
	$('.box-download0').on('click', function() {
		$('.modal-download0').fadeIn(300);
	});
	$('.modal-download0 .btn-close').on('click', function() {
		$('.modal-download0').fadeOut(300);
	});
	
	$('.box-download1').on('click', function() {
		$('.modal-download1').fadeIn(300);
	});
	$('.cont3_hide .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});
	
	function windAn() {
		if($.visionAn($('.sec7 .box-effect'), 0.9, 1)) {
			$('.sec7 .box-wind').addClass('windAn');
		};
	};
	windAn();
	$(window).on('scroll', function() {
		windAn();
	});

	// card effect
	setTimeout(function() {
		$.cardAn($('.sec7 .card-item1'), 0.572, 0.882, 0.882);
		$.cardAn($('.sec7 .card-item2'), 0.872, 0.922, 0.922);
		$.cardAn($('.sec7 .card-item3'), 0.872, 0.982, 0.982);
		$(window).on('scroll', function() {
			$.cardAn($('.sec7 .card-item1'), 0.572, 0.882, 0.882);
			$.cardAn($('.sec7 .card-item2'), 0.872, 0.922, 0.922);
			$.cardAn($('.sec7 .card-item3'), 0.872, 0.982, 0.982);
		});
	}, 500);

	// // 淡入动画
	$.fadeUpAn($('.sec2 .cont2_dh1'));
	$.fadeUpAn($('.sec2 .cont2_dzhongt'));

	$.fadeUpAn($('.sec3 .cont3_dh1'));
	$.fadeUpAn($('.sec3 .cont3_dh2'));
	$.fadeUpAn($('.sec3 .cont3_p'));
	$.fadeUpAn($('.sec3 .cont3_rtimg'));
	$.fadeUpAn($('.sec4 .cont4_dh1'));
	$.fadeUpAn($('.sec4 .cont4_p'));
	$.fadeUpAn($('.sec4 .gd_ckgd'));
	$.fadeUpAn($('.sec5 .cont5_dh1'));
	$.fadeUpAn($('.sec5 .cont5_dh2'));
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .cont2_dh1'));
		$.fadeUpAn($('.sec2 .cont2_dzhongt'));
		$.fadeUpAn($('.sec3 .cont3_dh1'));
		$.fadeUpAn($('.sec3 .cont3_dh2'));
		$.fadeUpAn($('.sec3 .cont3_p'));
		$.fadeUpAn($('.sec3 .cont3_rtimg'));
		$.fadeUpAn($('.sec4 .cont4_dh1'));
		$.fadeUpAn($('.sec4 .cont4_p'));
		$.fadeUpAn($('.sec4 .gd_ckgd'));
		$.fadeUpAn($('.sec5 .cont5_dh1'));
		$.fadeUpAn($('.sec5 .cont5_dh2'));
	});
});