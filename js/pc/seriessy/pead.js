jQuery(function($) {
	// sec1
	// download
	$('.box-download0').on('click', function() {
		$('.modal-download0').fadeIn(300);
	});
	$('.modal-download0 .btn-close').on('click', function() {
		$('.modal-download0').fadeOut(300);
	});
	
	// // 淡入动画
	$.fadeUpAn($('.sec2 .cont2_dh1'));
	$.fadeUpAn($('.sec2 .cont2_p'));
	$.fadeUpAn($('.sec3 .cont3_dh1'));
	$.fadeUpAn($('.sec3 .cont3_p'));
	$.fadeUpAn($('.sec3 .cont3_img'));
	$.fadeUpAn($('.sec4 .cont4_dh1'));
	$.fadeUpAn($('.sec4 .cont4_p'));
	$.fadeUpAn($('.sec4 .cont4_dh2'));	
	$.fadeUpAn($('.sec5 .cont4_dh1'));
	$.fadeUpAn($('.sec5 .cont5_img'));	
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .cont2_dh1'));
		$.fadeUpAn($('.sec2 .cont2_p'));
		$.fadeUpAn($('.sec3 .cont3_dh1'));
		$.fadeUpAn($('.sec3 .cont3_p'));
		$.fadeUpAn($('.sec3 .cont3_img'));
		$.fadeUpAn($('.sec4 .cont4_dh1'));
		$.fadeUpAn($('.sec4 .cont4_p'));
		$.fadeUpAn($('.sec4 .cont4_dh2'));
		$.fadeUpAn($('.sec5 .cont4_dh1'));
		$.fadeUpAn($('.sec5 .cont5_img'));	

	});
});