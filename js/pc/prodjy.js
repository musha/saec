jQuery(function($) {
    // 淡入动画
    setTimeout(function() {
        $.fadeUpAn($('.sec1 .box-title1'));
        $.fadeUpAn($('.sec1 .content1'));
        $.fadeUpAn($('.sec2 .box-title2'));
        $.fadeUpAn($('.sec2 .box-select'));
        $.fadeUpAn($('.sec2 .box-area'));
        $.fadeUpAn($('.sec2 .btn-submit'));
        // scroll
        $(window).on('scroll', function() {
            $.fadeUpAn($('.sec1 .box-title1'));
            $.fadeUpAn($('.sec1 .content1'));
            $.fadeUpAn($('.sec2 .box-title2'));
            $.fadeUpAn($('.sec2 .box-select'));
            $.fadeUpAn($('.sec2 .box-area'));
            $.fadeUpAn($('.sec2 .btn-submit'));
        });
    }, 300);

    // sec1 
    // hover
    $('.sec1 .content div').hover(function() {
        $(this).addClass('shadow');
    }, function() {
        $(this).removeClass('shadow');
    });

    // sec2
    // 选好之前不能点击
    $('.sec2 .box-area select').prop('disabled', true);
    // 选机型
    $('.sec2 .prod-item').on('click', function() {
        $(this).addClass('active').siblings().removeClass('active');
    });
    // 选制冷/热面积
    $('.sec2 .area-item').on('click', function() {
        if($('.sec2 .prod-item.active').length == 0) {
            alert('请选择机型');
        } else {
            $(this).addClass('active').siblings().removeClass('active');
            $('.sec2 .box-area select').prop('disabled', false);
        };
    });
    // 选面积
    $('.sec2 .box-area select').on('change propertychange', function() {
        if($(this).val() != '') {
            $('.sec2 .box-area').addClass('active');
        } else {
            $('.sec2 .box-area').removeClass('active');
        };
    });
    // 确认
    $('.sec2 .btn-submit').on('click', function() {
        if($('.sec2 .prod-item.active').length == 0) {
            alert('请选择机型');
            return;
        };
        if($('.sec2 .area-item.active').length == 0) {
            alert('请选择制冷/热面积');
            return;
        };
        if($('.sec2 .box-area select').val() == '') {
            alert('请选择面积');
            return;
        };
        var screenSelect = {};
        screenSelect['areatype'] = parseInt($('.sec2 .area-item.active').data('type'));
        screenSelect['area'] = $('.sec2 .box-area select').val();
        sessionStorage.setItem('screenSelect', JSON.stringify(screenSelect));
        if($('.sec2 .prod-item.active').data('prod') == '壁挂机') {
            window.location.href = 'p.prodjybgmodel.html';
        } else {
            window.location.href = 'p.prodjygsmodel.html';
        };
    });
});