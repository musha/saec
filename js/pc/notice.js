jQuery(function($) {
	// 淡入动画
	// setTimeout(function() {
 //        $.fadeUpAn($('.sec1 .box-title1'));
	// 	$.fadeUpAn($('.sec1 .box-result'));
	// 	// scroll
	// 	$(window).on('scroll', function() {
	// 		$.fadeUpAn($('.sec1 .box-title1'));
 //            $.fadeUpAn($('.sec1 .box-result'));
	// 	});
	// }, 300);

	// 调取通知
    function getNotice() {
        $.ajax({
            type: 'get',
            url: 'http://saec.gypserver.com/service/api/Home/GetChannelContents',
            dataType: 'json',
            // contentType: 'application/json',
            data: {
                cnl: 12
            },
            // async: false,
            cache: false,
            success: function(data) {
                // console.log(data);
                if(data.success) {
                	$('.sec1 .content-result').empty();
                    $('.sec1 .tip-error').addClass('hide');
                 	var len = data.data.data.length;
                 	if(len > 0) {
                 		$('.sec1 .tip-no').addClass('hide');
                 		for(var i = 0; i < len; i++) {
                 			var $li = $('<li class="res-item bg-white"><div class="res-title clearfix pointer"><div class="title fl">' + data.data.data[i].title + '</div><div class="date fr">' + data.data.data[i].subTitle + '</div></div><div class="res-content hide">' + data.data.data[i].content + '</div></li>');
                 			$('.sec1 .content-result').append($li);
                 		}
                 	} else  {
                 		$('.sec1 .tip-no').removeClass('hide');
                 	};
                };
            },
            error: function() {
                // 错误提示
                $('.sec1 .tip-error').removeClass('hide');
            },
            complete: function(data) {
                // 调取完毕
                $('.modal-loading').fadeOut(300);
                // 内容折叠
                $('.sec1 .res-title').on('click', function() {
                    $(this).next().slideToggle();
                });
            }
        });
    };
    getNotice();
});