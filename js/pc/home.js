// 要等DOM加载完成
jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .box-other'));
		$.fadeUpAn($('.sec1 .box-zykt'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-player'));
		$.fadeUpAn($('.sec2 .box-title2-2'));
		$.fadeUpAn($('.sec2 .btn-more-l'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .btn-more-l'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .box-text'));
		$.fadeUpAn($('.sec4 .btn-more-l'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec1 .box-title1'));
			$.fadeUpAn($('.sec1 .box-other'));
			$.fadeUpAn($('.sec1 .box-zykt'));
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .box-player'));
			$.fadeUpAn($('.sec2 .box-title2-2'));
			$.fadeUpAn($('.sec2 .btn-more-l'));
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .btn-more-l'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .box-text'));
			$.fadeUpAn($('.sec4 .btn-more-l'));
		});
	}, 300);
});
// 要等图片加载完成
$(window).load(function() {
	// sec0 
	// swiper
	var swiper0 = new Swiper('.sec0 .swiper0', {
		loop: true,
		effect: 'fade',
		prevButton: '.sec0 .swiper-button-prev',
		nextButton: '.sec0 .swiper-button-next'
	});
	
	// player
	var playerboss = $('#playerboss'),
		playerhome = $('#playerhome'),
		playerfz = $('#playerfz'),
		playersjl = $('#playersjl'),
		playerbrand = $('#playerbrand');
	$('.sec0 .btn-play1').on('click', function() {
		$('.modal-boss').fadeIn(300);
		playerboss[0].play();
	});
	$('.sec0 .btn-play2').on('click', function() {
		$('.modal-home').fadeIn(300);
		playerhome[0].play();
	});
	$('.sec0 .btn-play3').on('click', function() {
		$('.modal-fz').fadeIn(300);
		playerfz[0].play();
	});
	$('.sec0 .btn-play4').on('click', function() {
		$('.modal-sjl').fadeIn(300);
		playersjl[0].play();
	});
	$('.sec2 .btn-player').on('click', function() {
		$('.modal-brand').fadeIn(300);
		playerbrand[0].play();
	});
	// close player
	$('.modal .btn-close').on('click', function() {
		if($(this).parent().next('video')[0].paused) {
			//
		} else {
			$(this).parent().next('video')[0].pause();
		};
		$('.modal').fadeOut(300);
	});
	
	// hover
	$('.sec0 .btn-play').hover(function() {
		$(this).attr('src', 'img/pc/home/btn-play-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/home/btn-play.png');
	});
	$('.btn-more-s').hover(function() {
		$(this).attr('src', 'img/pc/home/btn-more-s-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/home/btn-more-s.png');
	});
	$('.box-zykt img').hover(function() {
		$(this).attr('src', 'img/pc/home/btn-zykt-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/home/btn-zykt.png');
	});
	$('.btn-more-l').hover(function() {
		$(this).attr('src', 'img/pc/home/btn-more-l-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/home/btn-more-l.png');
	});
});