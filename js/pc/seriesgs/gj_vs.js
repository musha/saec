jQuery(function ($) {
    sec1();
    sec3();
    sec4();
    sec6();

    $(window).on('scroll', function () {
        $.fadeUpAn($('.sec3 .titleicon'));
        $.fadeUpAn($('.sec3 .title'));
        $.fadeUpAn($('.sec3 .swiperTitle'));
        $.fadeUpAn($('.sec3 .swiper3'));
        $.fadeUpAn($('.sec5 .titleicon'));
        $.fadeUpAn($('.sec5 .title'));
        $.fadeUpAn($('.sec5 .sec5Img'));
        $.fadeUpAn($('.sec5 .sec5Txt'));
        $.fadeUpAn($('.sec4 .titleicon'));
        $.fadeUpAn($('.sec4 .title'));
        $.fadeUpAn($('.sec4 .swiper4'));
        
        $.fadeUpAn($('.sec4 .a1'));
        $.fadeUpAn($('.sec6 .titleicon'));
        $.fadeUpAn($('.sec6 .swiper6'));
    })
})

function sec1() {
    $('.color-item').on('click', function () {
        $(this).addClass("active").siblings().removeClass("active");
        $("#section1_productImg").attr('src', 'img/pc/seriesgs/vs/section1_product_' + $(this).attr('data-type') + '.png');
    });

    // download
    $('.box-download').on('click', function () {
        $('.modal-download').fadeIn(300);
    });

    // close modal
    $('.modal .btn-close').on('click', function () {
        $('.modal').fadeOut(300);
    });

    // 180 check
    $.prod180An('box-vs', $('#box-vs'), '3d/180/gs/VS/VS/0_', 0, 36);

    // 180 modal
    $('.sec1 .btn-180').on('click', function () {
        $('.modal-vs').fadeIn(300);
    });
}

function sec3() {
    var swiper3 = new Swiper('.sec3 .swiper3', {
        loop: true,
        initialSlide: 0,
        centeredSlides: true,
        prevButton: '.swiper3-button-prev',
        nextButton: '.swiper3-button-next',
        slidesPerView: 2,
        spaceBetween: '10%',
        effect: "coverflow",
        coverflow: {
            rotate: 0,
            stretch: -50, // 值越小两边的slide越靠近中间的slide
            depth: 50, // 值越大两边的slide越小
            modifier: 10, // 值越小两边的slide越小
            slideShadows: false
        },
        onSlideChangeEnd: function (swiper) {
            // console.log(swiper.realIndex);
            if (swiper.realIndex == 0) {
                $('.sec3 .bg .sec3bg_cold').fadeIn();
                $('.sec3 .bg .sec3bg_hot').fadeOut();
                $('.sec3 .swiperTitle img').attr('src', 'img/pc/seriesgs/vs/section3_title_cold.png')
                $('.sec3 .swiperTxt .swiperTxten').attr('src', 'img/pc/seriesgs/section3/section3_cool_en.png')
                $('.sec3 .swiperTxt .swiperTxtzh').attr('src', 'img/pc/seriesgs/section3/section3_cool_zh.png')
                $('.sec3 .swiperTxt8400 img').attr('src', 'img/pc/seriesgs/vs/section3_txt_8000.png')
                $('.sec3 .swiperSelect img').attr('src', 'img/pc/seriesgs/xs_sxs/section3_select_cold.png')
            }
            if (swiper.realIndex == 1) {
                $('.sec3 .bg .sec3bg_cold').fadeOut();
                $('.sec3 .bg .sec3bg_hot').fadeIn();
                $('.sec3 .swiperTitle img').attr('src', 'img/pc/seriesgs/vs/section3_title_hot.png')
                $('.sec3 .swiperTxt .swiperTxten').attr('src', 'img/pc/seriesgs/section3/section3_warm_en.png')
                $('.sec3 .swiperTxt .swiperTxtzh').attr('src', 'img/pc/seriesgs/section3/section3_warm_zh.png')
                $('.sec3 .swiperTxt8400 img').attr('src', 'img/pc/seriesgs/vs/section3_txt_10300.png')
                $('.sec3 .swiperSelect img').attr('src', 'img/pc/seriesgs/xs_sxs/section3_select_hot.png')
            }
        }
    });
}

function sec4() {
    var swiper4 = new Swiper('.sec4 .swiper4', {
        initialSlide: 0,
        loop: true,
        effect: 'fade',
        pagination: '.swiper4-pagination',
        prevButton: '.swiper4-button-prev',
        nextButton: '.swiper4-button-next',
        onSlideChangeEnd: function (swiper) {
            // switch (swiper.realIndex) {
            //     case 0:
            //         $('.sec4 .infoBox img').attr('src', "img/pc/seriesbg/gj/vs/section4_1bgfont1.png")
            //         break;
            //     case 1:
            //         $('.sec4 .infoBox img').attr('src', "img/pc/seriesbg/gj/vs/section4_1bgfont2.png")
            //         break;
            //     case 2:
            //         $('.sec4 .infoBox img').attr('src', "img/pc/seriesbg/gj/vs/section4_1bgfont3.png")
            //         break;
            //     case 3:
            //         $('.sec4 .infoBox img').attr('src', "img/pc/seriesbg/gj/vs/section4_1bgfont4.png")
            //         break;
            //     case 4:
            //         $('.sec4 .infoBox img').attr('src', "img/pc/seriesbg/gj/vs/section4_1bgfont5.png")
            //         break;
            // }
        }
    });
}

function sec6() {
    var swiper6 = new Swiper('.sec6 .swiper6', {
        loop: true,
        initialSlide: 0,
        centeredSlides: true,
        pagination: '.swiper6-pagination',
        prevButton: '.swiper6-button-prev',
        nextButton: '.swiper6-button-next',
        slidesPerView: 2,
        // spaceBetween: '10%',
        effect: "coverflow",
        coverflow: {
            rotate: 0,
            stretch: -50, // 值越小两边的slide越靠近中间的slide
            depth: 50, // 值越大两边的slide越小
            modifier: 10, // 值越小两边的slide越小
            slideShadows: false
        }
    });
}
