// 要等DOM加载完成
jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .text2-1'));
		$.fadeUpAn($('.sec2 .text2-2'));
		$.fadeUpAn($('.sec2 .text2-3'));
		$.fadeUpAn($('.sec2 .swiper-container'));
		$.fadeUpAn($('.sec2 .box-type'));
		$.fadeUpAn($('.sec2 .swiper-button-prev'));
		$.fadeUpAn($('.sec2 .swiper-button-next'));
		$.fadeUpAn($('.sec2 .swiper-pagination'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .text3'));
		$.fadeUpAn($('.sec3 .swiper-button-prev'));
		$.fadeUpAn($('.sec3 .swiper-button-next'));
		$.fadeUpAn($('.sec3 .swiper-pagination'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .swiper-container'));
		$.fadeUpAn($('.sec4 .swiper-button-prev'));
		$.fadeUpAn($('.sec4 .swiper-button-next'));
		$.fadeUpAn($('.sec4 .swiper-pagination'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .text2-1'));
			$.fadeUpAn($('.sec2 .text2-2'));
			$.fadeUpAn($('.sec2 .text2-3'));
			$.fadeUpAn($('.sec2 .swiper-container'));
			$.fadeUpAn($('.sec2 .box-type'));
			$.fadeUpAn($('.sec2 .swiper-button-prev'));
			$.fadeUpAn($('.sec2 .swiper-button-next'));
			$.fadeUpAn($('.sec2 .swiper-pagination'));
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .text3'));
			$.fadeUpAn($('.sec3 .swiper-button-prev'));
			$.fadeUpAn($('.sec3 .swiper-button-next'));
			$.fadeUpAn($('.sec3 .swiper-pagination'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .swiper-container'));
			$.fadeUpAn($('.sec4 .swiper-button-prev'));
			$.fadeUpAn($('.sec4 .swiper-button-next'));
			$.fadeUpAn($('.sec4 .swiper-pagination'));
		});
	}, 300);

	// sec1
	// download
	$('.sec1 .box-download').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// 切换产品 初始值是1，第一个的颜色
	$('.sec1 .color-item' + $('.sec1 .item-prod img').data('index')).addClass('active');
	$('.sec1 .btn-360').attr('href', '3d/360/gs/XFJ SXFJ/XFJ/index.html');
	$('.sec1 .color-item').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec1 .item-prod img').data('index', $(this).data('color'));
		$('.sec1 .item-prod img').attr('src', 'img/pc/seriesgs/xfj/prod1-' + $(this).data('color') + '.png');
		// 改变360链接
		switch(parseInt($(this).data('color'))) {
			case 1:
				$('.sec1 .btn-360').attr('href', '3d/360/gs/XFJ SXFJ/XFJ/index.html');
				break;
			case 2:
				$('.sec1 .btn-360').attr('href', '3d/360/gs/XFJ SXFJ/SXFJ/index.html');
				break;
			default:
				//
		};
	});
	// 180 查看
	$.prod180An('box-xfj', $('#box-xfj'), '3d/180/gs/XFJ SXFJ/XFJ/XF00', 0, 36);
    $.prod180An('box-sxfj', $('#box-sxfj'), '3d/180/gs/XFJ SXFJ/SXFJ/0_', 0, 36);
	// 180 弹窗
	$('.sec1 .btn-180').on('click', function() {
		switch(parseInt($('.sec1 .item-prod img').data('index'))) {
			case 1:
				$('.modal-xfj').fadeIn(300);
				break;
			case 2:
				$('.modal-sxfj').fadeIn(300);
				break;
			default:
				//
		};
	});
	// 关闭弹窗
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec2
	var typeArr = ['制冷', '制热'];
	// swiper prod
	var swiper2 = new Swiper('.sec2 .swiper2', {
		noSwiping: true,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 154, /* 值越小两边的slide越靠近中间的slide */
            depth: 70,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		prevButton: '.sec2 .swiper-button-prev',
		nextButton: '.sec2 .swiper-button-next',
		pagination: '.sec2 .swiper-pagination',
		paginationClickable: true,
		paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><span>' + typeArr[index] + '</span></div>';
        },
		onSlideChangeEnd: function(swiper) {
			$('.sec2 .bg-item' + (swiper.realIndex + 1)).addClass('bgAn1').removeClass('bgAn2').siblings().removeClass('bgAn1').addClass('bgAn2');
			switch(swiper.realIndex) {
				case 0:
					$('.sec2 .box-title2').addClass('colorcold').removeClass('colorhot');
					$('.sec2 .box-text2-1').addClass('active');
					$('.sec2 .box-text2-2').removeClass('active');
					$('.sec2 .type-item1').addClass('active').siblings().removeClass('active');
					$('.sec2 .swiper2 .swiper-pagination').addClass('colorcold').removeClass('colorhot');
					break;
				case 1:
					$('.sec2 .box-title2').addClass('colorhot').removeClass('colorcold');
					$('.sec2 .box-text2-2').addClass('active');
					$('.sec2 .box-text2-1').removeClass('active');
					$('.sec2 .type-item2').addClass('active').siblings().removeClass('active');
					$('.sec2 .swiper2 .swiper-pagination').addClass('colorhot').removeClass('colorcold');
					break;
				default:
					//
			};
		}
	});
	
	// sec4
	// swiper
	var swiper4 = new Swiper('.sec4 .swiper4', {
		noSwiping: true, // 禁止手滑
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: 80, /* 值越小两边的slide越靠近中间的slide */
            depth: 56,  /* 值越大两边的slide越小 */
            modifier: -5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        prevButton: '.sec4 .swiper-button-prev',
		nextButton: '.sec4 .swiper-button-next',
		pagination: '.sec4 .swiper-pagination'
	});
});
// 要等图片加载完成
$(window).load(function() {
	// sec3 
	// swiper
	var swiper3 = new Swiper('.sec3 .swiper3', {
		noSwiping: true,
		loop: true,
		effect: "fade",
		prevButton: '.sec3 .swiper-button-prev',
		nextButton: '.sec3 .swiper-button-next',
		pagination: '.sec3 .swiper-pagination'
	});
});