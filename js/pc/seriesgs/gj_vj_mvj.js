$(window).load(function() {
    sec1();
    sec3();
    sec4();
    sec6();

    setTimeout(function() {
        $.fadeUpAn($('.sec3 .titleicon'));
        $.fadeUpAn($('.sec3 .title'));
        $.fadeUpAn($('.sec3 .swiperTitle'));
        $.fadeUpAn($('.sec3 .swiper3'));
        $.fadeUpAn($('.sec3 .swiperTxt'));
        $.fadeUpAn($('.sec3 .swiperTxt8400'));
        $.fadeUpAn($('.sec3 .swiperSelect'));
        $.fadeUpAn($('.sec4 .titleicon'));
        $.fadeUpAn($('.sec4 .title'));
        $.fadeUpAn($('.sec4 .text4'));
        $.fadeUpAn($('.sec4 .swiper4-button-prev'));
        $.fadeUpAn($('.sec4 .swiper4-button-next'));
        $.fadeUpAn($('.sec4 .swiper4-pagination'));
        $.fadeUpAn($('.sec6 .titleicon'));
        $.fadeUpAn($('.sec6 .swiper6'));
        $.fadeUpAn($('.sec6 .swiper6-pagination'));
        $(window).on('scroll', function () {
            $.fadeUpAn($('.sec3 .titleicon'));
            $.fadeUpAn($('.sec3 .title'));
            $.fadeUpAn($('.sec3 .swiperTitle'));
            $.fadeUpAn($('.sec3 .swiper3'));
            $.fadeUpAn($('.sec3 .swiperTxt'));
            $.fadeUpAn($('.sec3 .swiperTxt8400'));
            $.fadeUpAn($('.sec3 .swiperSelect'));
            $.fadeUpAn($('.sec4 .titleicon'));
            $.fadeUpAn($('.sec4 .title'));
            $.fadeUpAn($('.sec4 .text4'));
            $.fadeUpAn($('.sec4 .swiper4-button-prev'));
            $.fadeUpAn($('.sec4 .swiper4-button-next'));
            $.fadeUpAn($('.sec4 .swiper4-pagination'));
            $.fadeUpAn($('.sec6 .titleicon'));
            $.fadeUpAn($('.sec6 .swiper6'));
            $.fadeUpAn($('.sec6 .swiper6-pagination'));
        });
    }, 300);
})

function sec1() {
    $('.sec1 .btn-360 a').attr('href', '3d/360/gs/VJ MVJ/VJ/index.html');
    $('.color-item').on('click', function () {
        $(this).addClass("active").siblings().removeClass("active");
        $("#section1_productImg").attr('src', 'img/pc/seriesgs/vj_mvj/section1_product_' + $(this).attr('data-type') + '.png');
        switch($(this).data('type')) {
            case 'b':
                $('.sec1 .btn-360 a').attr('href', '3d/360/gs/VJ MVJ/VJ/index.html');
                break;
            case 'y':
                $('.sec1 .btn-360 a').attr('href', '3d/360/gs/VJ MVJ/MVJ/index.html');
                break;
            default:
                //
        };
    });

    // download
    $('.box-download').on('click', function () {
        $('.modal-download').fadeIn(300);
    });

    // close modal
    $('.modal .btn-close').on('click', function () {
        $('.modal').fadeOut(300);
    });

    // 180 check
    $.prod180An('box-vj', $('#box-vj'), '3d/180/gs/VJ MVJ/VJ/0_', 0, 36);
    $.prod180An('box-mvj', $('#box-mvj'), '3d/180/gs/VJ MVJ/MVJ/0_', 0, 36);

    // 180 modal
    $('.sec1 .btn-180').on('click', function () {
        switch ($('.sec1 .color-item.active').data('type')) {
            case 'b':
                $('.modal-vj').fadeIn(300);
                break;
            case 'y':
                $('.modal-mvj').fadeIn(300);
                break;
            default:
                //
        };
    });
}

function sec3() {
    var swiper3 = new Swiper('.sec3 .swiper3', {
        loop: true,
        initialSlide: 0,
        centeredSlides: true,
        prevButton: '.swiper3-button-prev',
        nextButton: '.swiper3-button-next',
        slidesPerView: 2,
        spaceBetween: '10%',
        effect: "coverflow",
        coverflow: {
            rotate: 0,
            stretch: -50, // 值越小两边的slide越靠近中间的slide
            depth: 50, // 值越大两边的slide越小
            modifier: 10, // 值越小两边的slide越小
            slideShadows: false
        },
        onSlideChangeEnd: function (swiper) {
            // console.log(swiper.realIndex);
            if (swiper.realIndex == 0) {
                $('.sec3 .titleBox h3').addClass('colorcold').removeClass('colorhot');
                $('.sec3 .bg .sec3bg_cold').fadeIn();
                $('.sec3 .bg .sec3bg_hot').fadeOut();
                $('.sec3 .swiperTitle img').attr('src', 'img/pc/seriesgs/vj_mvj/section3_title_cold.png')
                $('.sec3 .swiperTxt .swiperTxten').attr('src', 'img/pc/seriesgs/section3/section3_cool_en.png')
                $('.sec3 .swiperTxt .swiperTxtzh').attr('src', 'img/pc/seriesgs/section3/section3_cool_zh.png')
                $('.sec3 .swiperTxt8400 img').attr('src', 'img/pc/seriesgs/vj_mvj/section3_txt_8000.png')
                $('.sec3 .swiperSelect img').attr('src', 'img/pc/seriesgs/xs_sxs/section3_select_cold.png')
            }
            if (swiper.realIndex == 1) {
                $('.sec3 .titleBox h3').addClass('colorhot').removeClass('colorcold');
                $('.sec3 .bg .sec3bg_cold').fadeOut();
                $('.sec3 .bg .sec3bg_hot').fadeIn();
                $('.sec3 .swiperTitle img').attr('src', 'img/pc/seriesgs/vj_mvj/section3_title_hot.png')
                $('.sec3 .swiperTxt .swiperTxten').attr('src', 'img/pc/seriesgs/section3/section3_warm_en.png')
                $('.sec3 .swiperTxt .swiperTxtzh').attr('src', 'img/pc/seriesgs/section3/section3_warm_zh.png')
                $('.sec3 .swiperTxt8400 img').attr('src', 'img/pc/seriesgs/vj_mvj/section3_txt_10300.png')
                $('.sec3 .swiperSelect img').attr('src', 'img/pc/seriesgs/xs_sxs/section3_select_hot.png')
            }
        }
    });
}

function sec4() {
    var swiper4 = new Swiper('.sec4 .swiper4', {
        initialSlide: 0,
        loop: true,
        effect: 'fade',
        pagination: '.swiper4-pagination',
        prevButton: '.swiper4-button-prev',
        nextButton: '.swiper4-button-next'
    });
}

function sec6() {
    var swiper6 = new Swiper('.sec6 .swiper6', {
        loop: true,
        initialSlide: 0,
        centeredSlides: true,
        pagination: '.swiper6-pagination',
        prevButton: '.swiper6-button-prev',
        nextButton: '.swiper6-button-next',
        slidesPerView: 2,
        // spaceBetween: '10%',
        effect: "coverflow",
        coverflow: {
            rotate: 0,
            stretch: -50, // 值越小两边的slide越靠近中间的slide
            depth: 50, // 值越大两边的slide越小
            modifier: 10, // 值越小两边的slide越小
            slideShadows: false
        }
    });
}
