jQuery(function($) {
    // 淡入动画
    setTimeout(function() {
        // $.fadeUpAn($('.sec1 .box-title'));
        // $.fadeUpAn($('.sec1 .swiper-pagination'));
        $.fadeUpAn($('.sec2 .box-title2'));
        $.fadeUpAn($('.sec2 .box-swiper'));
        $.fadeUpAn($('.sec3 .box-title3'));
        // scroll
        $(window).on('scroll', function() {
            // $.fadeUpAn($('.sec1 .box-title'));
            // $.fadeUpAn($('.sec1 .swiper-pagination'));
            $.fadeUpAn($('.sec2 .box-title2'));
            $.fadeUpAn($('.sec2 .box-swiper'));
            $.fadeUpAn($('.sec3 .box-title3'));
        });
    }, 300);

	// sec1 
	// swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		loop: true,
		effect: 'fade',
		prevButton: '.sec1 .swiper-button-prev',
		nextButton: '.sec1 .swiper-button-next',
		pagination: '.sec1 .swiper-pagination'
	});

	// sec2
	// 调接口获取文本
	$.ajax({
        type: 'get',
        url: 'http://saec.gypserver.com/service/api/Home/GetChannelContents',
        dataType: 'json',
        // contentType: 'application/json',
        data: {
            cnl: 8
        },
        // async: false,
        cache: false,
        success: function(data) {
            // console.log(data);
            if(data.success) {
                $('.sec2 .swiper2 .swiper-wrapper').empty();
                $('.sec2 .tip-error').addClass('hide');
                var len = data.data.data.length;
                if(len > 0) {
                    // 有数据
                    $('.sec2 .tip-no').addClass('hide');
                    var  yearArr = [];
                    // 加载年份
                    for(var i = 0; i < len; i++) {
                        yearArr.push(data.data.data[i].title);
                    	var $slide = $('<div class="swiper-slide"><div class="content center">' + data.data.data[i].content + '</div></div>');
                    	$('.sec2 .swiper2 .swiper-wrapper').append($slide);
                    };
                    // swiper
					var swiper2 = new Swiper('.sec2 .swiper2', {
                        parallax : true,
						pagination: '.sec2 .swiper-pagination',
						// paginationClickable: true,
						observer: true, //修改swiper自己或子元素时，自动初始化swiper
                        observeParents: false, //修改swiper的父元素时，自动初始化swiper
                        paginationBulletRender: function (swiper, index, className) {
                            return '<div class="' + className + '"><span>' + yearArr[index] + '</span><div class="line center"></div></div>';
                        }
					});
                } else {
                    // 无数据
                    $('.sec2 .tip-no').removeClass('hide');
                };
            };
        },
        error: function() {
            // 错误提示
            $('.sec2 .tip-error').removeClass('hide');
        },
        complete: function(data) {
            // 调取完毕
            $('.sec2 .modal-loading').fadeOut(300);
        }
    });
    
    // hover
    $('.btn-more-m').on('touchstart', function() {
        $(this).attr('src', '../img/mo/about/btn-more-m-active.png');
    });
});