jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .prod3'));
		$.fadeUpAn($('.sec3 .text3'));
		$.fadeUpAn($('.sec3 .card3-1'));
		$.fadeUpAn($('.sec3 .card3-2'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .text4-1'));
		$.fadeUpAn($('.sec4 .text4-2'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .text5-1'));
		$.fadeUpAn($('.sec5 .text5-2'));
		$.fadeUpAn($('.sec6 .box-title'));
		$.fadeUpAn($('.sec6 .text6-1'));
		$.fadeUpAn($('.sec6 .text6-2'));
		$.fadeUpAn($('.sec6 .text6-3'));
		$.fadeUpAn($('.sec6 .text6-4'));
		$.fadeUpAn($('.sec6 .box-option'));
		$.fadeUpAn($('.sec7 .box-title7'));
		$.fadeUpAn($('.sec7 .text7-1'));
		$.fadeUpAn($('.sec7 .text7-2'));
		$.fadeUpAn($('.sec8 .box-title8'));
		$.fadeUpAn($('.sec8 .text8-1'));
		$.fadeUpAn($('.sec8 .text8-2'));
		$.fadeUpAn($('.sec8 .box-swiper'));
		$.fadeUpAn($('.sec9 .box-title9'));
		$.fadeUpAn($('.sec9 .text9-1'));
		$.fadeUpAn($('.sec9 .text9-2'));
		$.fadeUpAn($('.sec9 .text9-3'));
		$.fadeUpAn($('.sec9 .text9-4'));
		$.fadeUpAn($('.sec9 .text9-5'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .prod3'));
			$.fadeUpAn($('.sec3 .text3'));
			$.fadeUpAn($('.sec3 .card3-1'));
			$.fadeUpAn($('.sec3 .card3-2'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .text4-1'));
			$.fadeUpAn($('.sec4 .text4-2'));
			$.fadeUpAn($('.sec5 .box-title5'));
			$.fadeUpAn($('.sec5 .text5-1'));
			$.fadeUpAn($('.sec5 .text5-2'));
			$.fadeUpAn($('.sec6 .box-title'));
			$.fadeUpAn($('.sec6 .text6-1'));
			$.fadeUpAn($('.sec6 .text6-2'));
			$.fadeUpAn($('.sec6 .text6-3'));
			$.fadeUpAn($('.sec6 .text6-4'));
			$.fadeUpAn($('.sec6 .box-option'));
			$.fadeUpAn($('.sec7 .box-title7'));
			$.fadeUpAn($('.sec7 .text7-1'));
			$.fadeUpAn($('.sec7 .text7-2'));
			$.fadeUpAn($('.sec8 .box-title8'));
			$.fadeUpAn($('.sec8 .text8-1'));
			$.fadeUpAn($('.sec8 .text8-2'));
			$.fadeUpAn($('.sec8 .box-swiper'));
			$.fadeUpAn($('.sec9 .box-title9'));
			$.fadeUpAn($('.sec9 .text9-1'));
			$.fadeUpAn($('.sec9 .text9-2'));
			$.fadeUpAn($('.sec9 .text9-3'));
			$.fadeUpAn($('.sec9 .text9-4'));
			$.fadeUpAn($('.sec9 .text9-5'));
		});
	}, 300);
	
	// sec1 
	// 180 check
	$.prod180An('box-fz', $('#box-fz'), '../3d/180/bg/FZ/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-180.modal-fz').fadeIn(300);
	});
	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});
	
	// sec2
	// player
	var playerfz;
	var getPlayerfz = setInterval(function() {
		playerfz = $('#ifrfz').contents().find('#playerfz');
		if(playerfz != undefined && playerfz != null) {
			if(playerfz.length > 0) {
				clearInterval(getPlayerfz);
				$('.sec2 .btn-player').on('click', function() {
					$('.modal-player.modal-fz').fadeIn(300);
					playerfz[0].play();
				});
			};
		};
	}, 300);

	// close player
	$('.modal-player .btn-close').on('click', function() {
		if($(this).parent().next().contents().find('video')[0].paused) {
			//
		} else {
			$(this).parent().next().contents().find('video')[0].pause();
		};
		$('.modal').fadeOut(300);
	});

	// sec5
	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec5 .box-cards'), $('.sec5 .content'), 0.94);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec5 .box-cards'), $('.sec5 .content'), 0.94);
		});
	}, 500);

	// sec6
	$('.sec6 .option-item1').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec6 .content-item1').addClass('active').next().removeClass('active');
		$('.sec6 .box-option').addClass('color1').removeClass('color2');
	});
	$('.sec6 .option-item2').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec6 .content-item2').addClass('active').prev().removeClass('active');
		$('.sec6 .box-option').addClass('color2').removeClass('color1');
	});

	// sec78
	// swiper 
	var swiper7 = new Swiper('.sec8 .swiper8', {
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		spaceBetween : 60,
        pagination: '.sec8 .swiper-pagination'
	});
});