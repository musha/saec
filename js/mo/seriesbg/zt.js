jQuery(function($) {
	// sec1 
	// swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		loop: true,
		slidesPerView: 'auto',
		centeredSlides : true,
		pagination: '.sec1 .swiper-pagination',
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: -3, /* 值越小两边的slide越靠近中间的slide */
            depth: 180,  /* 值越大两边的slide越小 */
            modifier: 3, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><div class="color' + (index + 1) + '"></div></div>';
        },
		onSlideChangeEnd: function(swiper) {
			// console.log(swiper.realIndex);
			// 改变360链接
			switch(swiper.realIndex) {
				case 0:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/ZT GZT/ZT/index.html');
					break;
				case 1:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/ZT GZT/GZT/index.html');
					break;
				default:
					//
			};
	    }
	});

	// 180 check
	$.prod180An('box-zt', $('#box-zt'), '../3d/180/bg/ZT GZT/ZT改/0_', 0, 36);
	$.prod180An('box-gzt', $('#box-gzt'), '../3d/180/bg/ZT GZT/GZT/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(swiper1.realIndex) {
			case 0:
				$('.modal-zt').fadeIn(300);
				break;
            case 1:
                $('.modal-gzt').fadeIn(300);
                break;
            default:
            //
		};
	});

	// download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec3 .box-text3-1'));
		$.fadeUpAn($('.sec3 .box-text3-2'));
		$.fadeUpAn($('.sec3 .box-qr'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .box-text'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .box-text5-1'));
		$.fadeUpAn($('.sec5 .box-text5-2'));
		$.fadeUpAn($('.sec5 .box-text5-3'));
		$.fadeUpAn($('.sec5 .box-card'));
		$.fadeUpAn($('.sec6 .box-title6'));
		$.fadeUpAn($('.sec6 .box-text6-1'));
		$.fadeUpAn($('.sec6 .box-text6-2'));
		$.fadeUpAn($('.sec6 .box-text6-3'));
		$.fadeUpAn($('.sec6 .box-text6-4'));
		$.fadeUpAn($('.sec7 .box-title7'));
		$.fadeUpAn($('.sec7 .text7-1'));
		$.fadeUpAn($('.sec7 .text7-2'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec3 .box-text3-1'));
			$.fadeUpAn($('.sec3 .box-text3-2'));
			$.fadeUpAn($('.sec3 .box-qr'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .box-text'));
			$.fadeUpAn($('.sec5 .box-title5'));
			$.fadeUpAn($('.sec5 .box-text5-1'));
			$.fadeUpAn($('.sec5 .box-text5-2'));
			$.fadeUpAn($('.sec5 .box-text5-3'));
			$.fadeUpAn($('.sec5 .box-card'));
			$.fadeUpAn($('.sec6 .box-title6'));
			$.fadeUpAn($('.sec6 .box-text6-1'));
			$.fadeUpAn($('.sec6 .box-text6-2'));
			$.fadeUpAn($('.sec6 .box-text6-3'));
			$.fadeUpAn($('.sec6 .box-text6-4'));
			$.fadeUpAn($('.sec7 .box-title7'));
			$.fadeUpAn($('.sec7 .text7-1'));
			$.fadeUpAn($('.sec7 .text7-2'));
	    });
	}, 300);

	// sec3
	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec3 .box-cards'), $('.sec3 .content'), 0.7);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec3 .box-cards'), $('.sec3 .content'), 0.7);
		});
    }, 500);
    
	// sec4
	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec4 .box-cards'), $('.sec4 .content'), 0.8);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec4 .box-cards'), $('.sec4 .content'), 0.8);
		});
	}, 500);

	// sec5
    function windAn() {
		if($.visionAn($('.sec5 .box-effect'), 0.9, 1)) {
            $('.sec5 .box-wind').animate({height: '393.97%'}, 3000);
		};
	}
	windAn();
	$(window).on('scroll', function() {
		windAn();
    });	
});