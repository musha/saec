jQuery(function($) {
	// sec1 
	// swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		loop: true,
		slidesPerView: 'auto',
		centeredSlides : true,
		pagination: '.sec1 .swiper-pagination',
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: -3, /* 值越小两边的slide越靠近中间的slide */
            depth: 180,  /* 值越大两边的slide越小 */
            modifier: 3, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><div class="color' + (index + 1) + '"></div></div>';
        }
	});

	// 180 check
	$.prod180An('box-ygj', $('#box-ygj'), '../3d/180/bg/YGJ SYGJ/YGJ/0_', 0, 36);
	$.prod180An('box-sygj', $('#box-sygj'), '../3d/180/bg/YGJ SYGJ/SYGJ/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(swiper1.realIndex) {
			case 0:
				$('.modal-ygj').fadeIn(300);
				break;
			case 1:
				$('.modal-sygj').fadeIn(300);
				break;
			default:
				//
		};
	});

	// download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-text'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .box-text'));
		$.fadeUpAn($('.sec3 .content'));
		$.fadeUpAn($('.sec4 .box-text'));
		$.fadeUpAn($('.sec4 .box-img'));
		$.fadeUpAn($('.sec4 .box-cards'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .box-text5-1'));
		$.fadeUpAn($('.sec5 .box-text5-2'));
	}, 300);
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-text'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .box-text'));
		$.fadeUpAn($('.sec3 .content'));
		$.fadeUpAn($('.sec4 .box-text'));
		$.fadeUpAn($('.sec4 .box-img'));
		$.fadeUpAn($('.sec4 .box-cards'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .box-text5-1'));
		$.fadeUpAn($('.sec5 .box-text5-2'));
	});

	// sec2
	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec2 .box-cards'), $('.sec2 .content'), 0.65);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec2 .box-cards'), $('.sec2 .content'), 0.65);
		});
	}, 500);

		
	// sec3 content
	$('.sec3 .btn-sleepmode-1').on('click', function() {
		$('.bg01,.btn-sleepmode-1,.sleepmodeclose').removeClass('show').addClass('hide');
		$('.bg02,.btn-sleepmode-2,.sleepmodeopen').removeClass('hide').addClass('show');
	});
	$('.sec3 .btn-sleepmode-2').on('click', function() {
		$('.bg02,.btn-sleepmode-2,.sleepmodeopen').removeClass('show').addClass('hide');
		$('.bg01,.btn-sleepmode-1,.sleepmodeclose').removeClass('hide').addClass('show');
	});
});