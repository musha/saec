jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .text2-1'));
		$.fadeUpAn($('.sec2 .text2-2'));
		$.fadeUpAn($('.sec2 .text2-3'));
		$.fadeUpAn($('.sec2 .text2-4'));
		$.fadeUpAn($('.sec2 .text2-5'));
		$.fadeUpAn($('.sec3 .slash3'));
		$.fadeUpAn($('.sec3 .title3'));
		$.fadeUpAn($('.sec3 .text3-1-1'));
		$.fadeUpAn($('.sec3 .text3-1-2'));
		$.fadeUpAn($('.sec3 .box-option'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .text4-1'));
		$.fadeUpAn($('.sec4 .text4-2'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .text2-1'));
			$.fadeUpAn($('.sec2 .text2-2'));
			$.fadeUpAn($('.sec2 .text2-3'));
			$.fadeUpAn($('.sec2 .text2-4'));
			$.fadeUpAn($('.sec2 .text2-5'));
			$.fadeUpAn($('.sec3 .slash3'));
			$.fadeUpAn($('.sec3 .title3'));
			$.fadeUpAn($('.sec3 .text3-1-1'));
			$.fadeUpAn($('.sec3 .text3-1-2'));
			$.fadeUpAn($('.sec3 .box-option'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .text4-1'));
			$.fadeUpAn($('.sec4 .text4-2'));
		});
	}, 300);

	// sec1 
	// 180 check
	$.prod180An('box-wgj', $('#box-wgj'), '../3d/180/bg/WGJ/WG00', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-wgj').fadeIn(300);
	});

	// download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec3 
	$('.sec3 .option-item1').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec3 .content-item1').addClass('active').siblings().removeClass('active');
	});
	$('.sec3 .option-item2').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec3 .content-item2').addClass('active').siblings().removeClass('active');
		if(!$('.sec3 .box-wind3-2').hasClass('windAn3-2')) {
			$('.sec3 .box-wind3-2').addClass('windAn3-2');
			$('.sec3 .box-wind3-2').animate({height: '24%'}, 3000);
		};
	});

	// wind effect
	function windAn3() {
		if($.visionAn($('.sec3 .box-wind3-1'), 0.7, 1)) {
			$('.sec3 .box-wind3-1').animate({height: '33%'}, 3000);
		};
	};
	windAn3();
	$(window).on('scroll', function() {
		windAn3();
	});
	
	// sec4
	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec4 .box-cards'), $('.sec4 .content'), 0.94);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec4 .box-cards'), $('.sec4 .content'), 0.94);
		});
	}, 500);
});