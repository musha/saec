jQuery(function($) {
	// sec1 swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		initialSlide: 1,
		loop: true,
		slidesPerView: 'auto',
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: -49, /* 值越小两边的slide越靠近中间的slide */
            depth: 130,  /* 值越大两边的slide越小 */
            modifier: 4, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		onSlideChangeEnd: function(swiper) {
			// console.log(swiper.realIndex);
			// 改变色块
			$('.sec1 .color-item' + (swiper.realIndex + 1)).addClass('active').siblings().removeClass('active');
	    }
	});

	// sec1 download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title'));
		$.fadeUpAn($('.sec2 .box-text2-1'));
		$.fadeUpAn($('.sec2 .box-text2-2'));
        $.fadeUpAn($('.sec3 .box-title'));
        $.fadeUpAn($('.sec3 .box-img'));
        $.fadeUpAn($('.sec3 .box-text'));
		$.fadeUpAn($('.sec4 .box-title4-1'));
		$.fadeUpAn($('.sec4 .box-title4-2'));
		$.fadeUpAn($('.sec4 .box-text4-1'));
		$.fadeUpAn($('.sec4 .box-text4-2'));
	}, 300);
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .box-title'));
		$.fadeUpAn($('.sec2 .box-text2-1'));
		$.fadeUpAn($('.sec2 .box-text2-2'));
		$.fadeUpAn($('.sec3 .box-title'));
		$.fadeUpAn($('.sec3 .box-img'));
		$.fadeUpAn($('.sec3 .box-text'));
		$.fadeUpAn($('.sec4 .box-title4-1'));
		$.fadeUpAn($('.sec4 .box-title4-2'));
		$.fadeUpAn($('.sec4 .box-text4-1'));
		$.fadeUpAn($('.sec4 .box-text4-2'));
	});
});