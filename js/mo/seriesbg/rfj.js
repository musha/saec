jQuery(function($) {

	// 180 check
	$.prod180An('box-rfj', $('#box-rfj'), '../3d/180/bg/RFJ/0_', 0, 36);

	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-rfj').fadeIn(300);
	});

	// sec1 download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-text'));
		$.fadeUpAn($('.sec2 .content'));
		$.fadeUpAn($('.sec3 .box-text'));
		$.fadeUpAn($('.sec3 .box-img'));
		$.fadeUpAn($('.sec3 .box-cards'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .box-text4-1'));
		$.fadeUpAn($('.sec4 .box-img'));
		$.fadeUpAn($('.sec4 .box-text4-2'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .box-text5-1'));
		$.fadeUpAn($('.sec5 .box-text5-2'));
	}, 300);
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-text'));
		$.fadeUpAn($('.sec2 .content'));
		$.fadeUpAn($('.sec3 .box-text'));
		$.fadeUpAn($('.sec3 .box-img'));
		$.fadeUpAn($('.sec3 .box-cards'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .box-text4-1'));
		$.fadeUpAn($('.sec4 .box-img'));
		$.fadeUpAn($('.sec4 .box-text4-2'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .box-text5-1'));
		$.fadeUpAn($('.sec5 .box-text5-2'));
	});

	// sec2 content
	$('.sec2 .btn-sleepmode-1').on('click', function() {
		$('.bg01,.btn-sleepmode-1,.sleepmodeclose').removeClass('show').addClass('hide');
		$('.bg02,.btn-sleepmode-2,.sleepmodeopen').removeClass('hide').addClass('show');
	});
	$('.sec2 .btn-sleepmode-2').on('click', function() {
		$('.bg02,.btn-sleepmode-2,.sleepmodeopen').removeClass('show').addClass('hide');
		$('.bg01,.btn-sleepmode-1,.sleepmodeclose').removeClass('hide').addClass('show');
	});
});