jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .prod2'));
		$.fadeUpAn($('.sec2 .text2-1'));
		$.fadeUpAn($('.sec2 .text2-2'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .text3-1'));
		$.fadeUpAn($('.sec3 .text3-2'));
		$.fadeUpAn($('.sec4 .title4-1'));
		$.fadeUpAn($('.sec4 .text4-1'));
		$.fadeUpAn($('.sec4 .text4-2'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .prod5'));
		$.fadeUpAn($('.sec5 .text5-1-1'));
		$.fadeUpAn($('.sec5 .text5-1-2'));
		$.fadeUpAn($('.sec5 .text5-2-1'));
		$.fadeUpAn($('.sec5 .text5-2-2'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .prod2'));
			$.fadeUpAn($('.sec2 .text2-1'));
			$.fadeUpAn($('.sec2 .text2-2'));
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .text3-1'));
			$.fadeUpAn($('.sec3 .text3-2'));
			$.fadeUpAn($('.sec4 .title4-1'));
			$.fadeUpAn($('.sec4 .text4-1'));
			$.fadeUpAn($('.sec4 .text4-2'));
			$.fadeUpAn($('.sec5 .box-title5'));
			$.fadeUpAn($('.sec5 .prod5'));
			$.fadeUpAn($('.sec5 .text5-1-1'));
			$.fadeUpAn($('.sec5 .text5-1-2'));
			$.fadeUpAn($('.sec5 .text5-2-1'));
			$.fadeUpAn($('.sec5 .text5-2-2'));
		});
	}, 300);

	// sec1 
	// swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		loop: true,
		slidesPerView: 'auto',
		centeredSlides : true,
		pagination: '.sec1 .swiper-pagination',
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: -3, /* 值越小两边的slide越靠近中间的slide */
            depth: 180,  /* 值越大两边的slide越小 */
            modifier: 3, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><div class="color' + (index + 1) + '"></div></div>';
        },
		onSlideChangeEnd: function(swiper) {
			// console.log(swiper.realIndex);
			// 改变360链接
			switch(swiper.realIndex) {
				case 0:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/ZHJ PZHJ AHJ PAHJ/ZHJ/index.html');
					break;
				case 1:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/ZHJ PZHJ AHJ PAHJ/PZHJ/index.html');
					break;
				default:
					//
			};
	    }
	});

	// 180 check
	$.prod180An('box-ahj', $('#box-ahj'), '../3d/180/bg/ZHJ PZHJ AHJ PAHJ/ZHJ AHJ/ZHJ00', 0, 36);
	$.prod180An('box-pahj', $('#box-pahj'), '../3d/180/bg/ZHJ PZHJ AHJ PAHJ/PZHJ PAHJ/0_', 0, 36)
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(swiper1.realIndex) {
			case 0:
				$('.modal-ahj').fadeIn(300);
				break;
			case 1:
				$('.modal-pahj').fadeIn(300);
				break;
			default:
				//
		};
	});

	// download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});
	
	// sec3
	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec3 .box-cards'), $('.sec3 .content'), 0.88);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec3 .box-cards'), $('.sec3 .content'), 0.88);
		});
	}, 500);

	// sec4
	// swiper
	var swiper4 = new Swiper('.sec4 .swiper4', {
		loop: true,
		effect: 'fade',
        pagination: '.sec4 .swiper-pagination'
	});

	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec4 .box-cards'), $('.sec4 .content'), 0.94);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec4 .box-cards'), $('.sec4 .content'), 0.94);
		});
	}, 500);
});