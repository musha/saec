jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-prod'));
		$.fadeUpAn($('.sec2 .box-text'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .swiperprod'));
		$.fadeUpAn($('.sec4 .box-info-swiper'));
		$.fadeUpAn($('.sec4 .swiper-pagination'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .swiperprod'));
		$.fadeUpAn($('.sec5 .swiper-pagination'));
		$.fadeUpAn($('.sec6 .title6'));
		$.fadeUpAn($('.sec6 .divider'));
		$.fadeUpAn($('.sec6 .subtitle6'));
		$.fadeUpAn($('.sec6 .prod6-1'));
		$.fadeUpAn($('.sec6 .prod6-2'));
		$.fadeUpAn($('.sec6 .prod6-3'));
		$.fadeUpAn($('.sec6 .prod6-4'));
		$.fadeUpAn($('.sec7 .box-title7'));
		$.fadeUpAn($('.sec7 .text7-1'));
		$.fadeUpAn($('.sec7 .text7-2'));
		$.fadeUpAn($('.sec8 .box-title8'));
		$.fadeUpAn($('.sec8 .text8-1'));
		$.fadeUpAn($('.sec8 .divider'));
		$.fadeUpAn($('.sec8 .text8-2'));
		$.fadeUpAn($('.sec9 .box-title9'));
		$.fadeUpAn($('.sec9 .text9-1'));
		$.fadeUpAn($('.sec9 .prod9'));
		$.fadeUpAn($('.sec9 .divider'));
		$.fadeUpAn($('.sec9 .text9-2'));
		$.fadeUpAn($('.sec10 .box-title10'));
		$.fadeUpAn($('.sec10 .text10-1'));
		$.fadeUpAn($('.sec10 .text10-2'));
		$.fadeUpAn($('.sec11 .box-title11'));
		$.fadeUpAn($('.sec11 .text11-1'));
		$.fadeUpAn($('.sec11 .divider'));
		$.fadeUpAn($('.sec11 .text11-2'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .box-prod'));
			$.fadeUpAn($('.sec2 .box-text'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .swiperprod'));
			$.fadeUpAn($('.sec4 .box-info-swiper'));
			$.fadeUpAn($('.sec4 .swiper-pagination'));
			$.fadeUpAn($('.sec5 .box-title5'));
			$.fadeUpAn($('.sec5 .swiperprod'));
			$.fadeUpAn($('.sec5 .swiper-pagination'));
			$.fadeUpAn($('.sec6 .title6'));
			$.fadeUpAn($('.sec6 .divider'));
			$.fadeUpAn($('.sec6 .subtitle6'));
			$.fadeUpAn($('.sec6 .prod6-1'));
			$.fadeUpAn($('.sec6 .prod6-2'));
			$.fadeUpAn($('.sec6 .prod6-3'));
			$.fadeUpAn($('.sec6 .prod6-4'));
			$.fadeUpAn($('.sec7 .box-title7'));
			$.fadeUpAn($('.sec7 .text7-1'));
			$.fadeUpAn($('.sec7 .text7-2'));
			$.fadeUpAn($('.sec8 .box-title8'));
			$.fadeUpAn($('.sec8 .text8-1'));
			$.fadeUpAn($('.sec8 .divider'));
			$.fadeUpAn($('.sec8 .text8-2'));
			$.fadeUpAn($('.sec9 .box-title9'));
			$.fadeUpAn($('.sec9 .text9-1'));
			$.fadeUpAn($('.sec9 .prod9'));
			$.fadeUpAn($('.sec9 .divider'));
			$.fadeUpAn($('.sec9 .text9-2'));
			$.fadeUpAn($('.sec10 .box-title10'));
			$.fadeUpAn($('.sec10 .text10-1'));
			$.fadeUpAn($('.sec10 .text10-2'));
			$.fadeUpAn($('.sec11 .box-title11'));
			$.fadeUpAn($('.sec11 .text11-1'));
			$.fadeUpAn($('.sec11 .divider'));
			$.fadeUpAn($('.sec11 .text11-2'));
		});
	}, 300);

	// sec1 
	// swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		loop: true,
		slidesPerView: 'auto',
		centeredSlides : true,
		pagination: '.sec1 .swiper-pagination',
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: -3, /* 值越小两边的slide越靠近中间的slide */
            depth: 180,  /* 值越大两边的slide越小 */
            modifier: 3, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><div class="color' + (index + 1) + '"></div></div>';
        },
		onSlideChangeEnd: function(swiper) {
			// console.log(swiper.realIndex);
			// 改变360链接
			switch(swiper.realIndex) {
				case 0:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/JL SJL GJL/JL/index.html');
					break;
				case 1:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/JL SJL GJL/SJL/index.html');
					break;
				case 2:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/JL SJL GJL/GJL/index.html');
					break;
				default:
					//
			};
	    }
	});

	// 180 check
	$.prod180An('box-jl', $('#box-jl'), '../3d/180/bg/JL SJL GJL/JL/0_', 1, 37);
	$.prod180An('box-sjl', $('#box-sjl'), '../3d/180/bg/JL SJL GJL/SJL/0_', 0, 36);
	$.prod180An('box-gjl', $('#box-gjl'), '../3d/180/bg/JL SJL GJL/GJL/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(swiper1.realIndex) {
			case 0:
				$('.modal-180.modal-jl').fadeIn(300);
				break;
			case 1:
				$('.modal-180.modal-sjl').fadeIn(300);
				break;
			case 2:
				$('.modal-180.modal-gjl').fadeIn(300);
				break;
			default:
				//
		};
	});

	// download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec2 
	$('.sec2 .btn-check-plus').on('click', function() {
		$('.sec2 .text2-1').addClass('hide').siblings().removeClass('hide');
	});
	$('.sec2 .text2-2').on('click', function() {
		$(this).addClass('hide').siblings().removeClass('hide');
	});

	// sec3
	// player
	var playerjl;
	var getPlayerjl = setInterval(function() {
		playerjl = $('#ifrjl').contents().find('#playerjl');
		if(playerjl != undefined && playerjl != null) {
			if(playerjl.length > 0) {
				clearInterval(getPlayerjl);
				$('.sec3 .btn-player').on('click', function() {
					$('.modal-player.modal-jl').fadeIn(300);
					playerjl[0].play();
				});
			};
		};
	}, 300);

	// close player
	$('.modal-player .btn-close').on('click', function() {
		if($(this).parent().next().contents().find('video')[0].paused) {
			//
		} else {
			$(this).parent().next().contents().find('video')[0].pause();
		};
		$('.modal').fadeOut(300);
	});
	
	// sec4
	// swiper bg
	var swiperbg4 = new Swiper('.sec4 .swiperbg', {
		noSwiping: true,
		loop: true,
		effect: 'fade'
	});
	// swiper prod
	var swiperprod4 = new Swiper('.sec4 .swiperprod', {
		loop: true,
		pagination: '.sec4 .swiper-paginationprod'
	});
	// swiper info
	var swiperinfo = new Swiper('.sec4 .swiperinfo', {
		noSwiping: true,
		loop: true,
		onSlideChangeEnd: function(swiper) {
			switch(swiper.realIndex) {
				case 0:
					$('.sec4 .text4-1').addClass('hide').next().removeClass('hide');
					$('.sec4 .text4-2').removeClass('hide').next().addClass('hide');
					$('.sec4 .text4-3').removeClass('hide').next().addClass('hide');
					break;
				case 1:
					$('.sec4 .text4-2').addClass('hide').next().removeClass('hide');
					$('.sec4 .text4-1').removeClass('hide').next().addClass('hide');
					$('.sec4 .text4-3').removeClass('hide').next().addClass('hide');
					break;
				case 2:
					$('.sec4 .text4-3').addClass('hide').next().removeClass('hide');
					$('.sec4 .text4-1').removeClass('hide').next().addClass('hide');
					$('.sec4 .text4-2').removeClass('hide').next().addClass('hide');
					break;
				default:
					//
			};
		}
	});

	// sec5
	// swiper bg
	var swiperbg5 = new Swiper('.sec5 .swiperbg', {
		noSwiping: true,
		loop: true,
		effect: 'fade'
	});
	var seriesArr = ['JL', 'SJL', 'GJL'];
	// swiper prod
	var swiperprod5 = new Swiper('.sec5 .swiperprod', {
		loop: true,
		pagination: '.sec5 .swiper-paginationprod',
		paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><span class="series">' + seriesArr[index] + '</span><span class="panel">系列面板</span><div class="line center"></div></div>';
        },
		onSlideChangeEnd: function(swiper) {
			$('.sec5 .title-item' + (swiper.realIndex + 1)).removeClass('hide').siblings().addClass('hide');
			$('.sec11 .prod11').attr('src', '../img/mo/seriesbg/jl/prod11-' + (swiper.realIndex + 1) + '.png');
			switch(swiper.realIndex) {
				case 0:
					$('.sec5 .swiperprod .swiper-pagination').addClass('color1').removeClass('color2').removeClass('color3');
					break;
				case 1:
				$('.sec5 .swiperprod .swiper-pagination').addClass('color2').removeClass('color1').removeClass('color3');
					break;
				case 2:
				$('.sec5 .swiperprod .swiper-pagination').addClass('color3').removeClass('color1').removeClass('color2');
					break;
				default:
					//
			};
		}
	});

	// swiper control
	swiperprod4.params.control = [swiperbg4, swiperinfo, swiperprod5];
	swiperprod5.params.control = [swiperbg5, swiperprod4];

	// sec7
	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec7 .box-cards'), $('.sec7 .content'), 0.9);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec7 .box-cards'), $('.sec7 .content'), 0.9);
		});
	}, 500);

	// sec8 
	// wind effect
	function windAn() {
		if($.visionAn($('.sec8 .box-effect'), 0.92, 1)) {
			$('.sec8 .box-wind').animate({height: '293.97%'}, 3000);
		};
	}
	windAn();
	$(window).on('scroll', function() {
		windAn();
	});

	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec8 .box-cards'), $('.sec8 .content'), 0.94);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec8 .box-cards'), $('.sec8 .content'), 0.94);
		});
	}, 500);
});