jQuery(function($) {
	// sec1 
	// swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		loop: true,
		slidesPerView: 'auto',
		centeredSlides : true,
		pagination: '.sec1 .swiper-pagination',
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: -3, /* 值越小两边的slide越靠近中间的slide */
            depth: 180,  /* 值越大两边的slide越小 */
            modifier: 3, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><div class="color' + (index + 1) + '"></div></div>';
        },
		onSlideChangeEnd: function(swiper) {
			// console.log(swiper.realIndex);
			// 改变360链接
			switch(swiper.realIndex) {
				case 0:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/ZFJ PZFJ/ZFJ/index.html');
					break;
				case 1:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/ZFJ PZFJ/PZFJ/index.html');
					break;
				default:
					//
			};
	    }
	});

	// 180 check
	$.prod180An('box-zfj', $('#box-zfj'), '../3d/180/bg/ZFJ PZFJ/ZFJ/ZFJ00', 0, 36);
	$.prod180An('box-pzfj', $('#box-pzfj'), '../3d/180/bg/ZFJ PZFJ/PZFJ/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(swiper1.realIndex) {
			case 0:
				$('.modal-zfj').fadeIn(300);
				break;
            case 1:
                $('.modal-pzfj').fadeIn(300);
                break;
            default:
            //
		};
	});

	// download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-img2'));
		$.fadeUpAn($('.sec2 .box-text2-1'));
		$.fadeUpAn($('.sec2 .box-text2-2'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .box-text'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .box-text4-1'));
		$.fadeUpAn($('.sec4 .box-img'));
		$.fadeUpAn($('.sec4 .box-text4-2'));
		$.fadeUpAn($('.sec4 .box-card'));
	}, 300);
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-img2'));
		$.fadeUpAn($('.sec2 .box-text2-1'));
		$.fadeUpAn($('.sec2 .box-text2-2'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .box-text'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .box-text4-1'));
		$.fadeUpAn($('.sec4 .box-img'));
		$.fadeUpAn($('.sec4 .box-text4-2'));
		$.fadeUpAn($('.sec4 .box-card'));
    });
    
    // sec2
	// swiper bg
	// var swiperbg = new Swiper('.sec2 .swiperbg', {
	// 	loop: true,
	// 	effect: 'fade'
	// });
	// var swiperimg = new Swiper('.sec2 .swiperimg', {
	// 	noSwiping: true,
	// 	loop: true,
    //     pagination: '.swiper-paginationimg',
    //     onSlideChangeEnd: function(swiper) {
	// 		$('.sec2 .text2-1').attr('src', '../img/mo/seriesbg/zfj/text2-1-' + (swiper.realIndex + 1) + '.png');
	// 		$('.sec2 .text2-2').attr('src', '../img/mo/seriesbg/zfj/text2-2-' + (swiper.realIndex + 1) + '.png');
	// 		switch(swiper.realIndex) {
	// 			case 0:
	// 				$('.sec2 .divider').addClass('color1').removeClass('color2');
	// 				break;
	// 			case 1:
	// 				$('.sec2 .divider').addClass('color2').removeClass('color1');
	// 				break;
	// 			default:
	// 				//
	// 		};
	// 	}   
	// });

    // swiper control
	// swiperimg.params.control = [swiperbg];

	// sec3
	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec3 .box-cards'), $('.sec3 .content'), 0.65);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec3 .box-cards'), $('.sec3 .content'), 0.65);
		});
	}, 500);
});