jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .text2-1'));
		$.fadeUpAn($('.sec2 .text2-2'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .text3-1'));
		$.fadeUpAn($('.sec3 .text3-2'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .text4'));
		$.fadeUpAn($('.sec5 .box-title5'));
		$.fadeUpAn($('.sec5 .prod5'));
		$.fadeUpAn($('.sec5 .text5-2-1'));
		$.fadeUpAn($('.sec6 .box-title6'));
		$.fadeUpAn($('.sec6 .text6-1'));
		$.fadeUpAn($('.sec6 .text6-2'));
		// scroll
		$(window).on('scroll', function() {
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .text2-1'));
			$.fadeUpAn($('.sec2 .text2-2'));
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .text3-1'));
			$.fadeUpAn($('.sec3 .text3-2'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .text4'));
			$.fadeUpAn($('.sec5 .box-title5'));
			$.fadeUpAn($('.sec5 .prod5'));
			$.fadeUpAn($('.sec5 .text5-2-1'));
			$.fadeUpAn($('.sec6 .box-title6'));
			$.fadeUpAn($('.sec6 .text6-1'));
			$.fadeUpAn($('.sec6 .text6-2'));
		});
	}, 300);

	// sec1 
	// 180 check
	$.prod180An('box-kt', $('#box-kt'), '../3d/180/bg/KT/0_', 1, 37);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-kt').fadeIn(300);
	});

	// download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec2 
	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec2 .box-cards'), $('.sec2 .content'), 0.8);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec2 .box-cards'), $('.sec2 .content'), 0.8);
		});
	}, 500);

	// sec3 
	// wind effect
	function windAn3() {
		if($.visionAn($('.sec3 .box-effect'), 0.8, 1)) {
			$('.sec3 .box-wind').animate({height: '504%'}, 3000);
		};
	};
	windAn3();
	$(window).on('scroll', function() {
		windAn3();
	});
	
	// sec4
	// wind effect
	function windAn4() {
		if($.visionAn($('.sec4 .box-effect'), 0.8, 1)) {
			$('.sec4 .box-wind').animate({height: '463%'}, 3000);
		};
	};
	windAn4();
	$(window).on('scroll', function() {
		windAn4();
	});

	// card effect
	setTimeout(function() {
		$.opacityAn($('.sec4 .box-cards'), $('.sec4 .content'), 0.94);
		$(window).on('scroll', function() {
			$.opacityAn($('.sec4 .box-cards'), $('.sec4 .content'), 0.94);
		});
	}, 500);

	// sec5
	// text effect
	function textAn() {
		if($.visionAn($('.sec5 .text5-1'), 0.8, 1)) {
			$('.sec5 .text5-1-1').addClass('animated fadeInUp');
			setTimeout(function() {
				$('.sec5 .text5-1-2').addClass('animated fadeInUp');
			}, 500);
			setTimeout(function() {
				$('.sec5 .text5-1-3').addClass('animated fadeInUp');
			}, 1000);
			setTimeout(function() {
				$('.sec5 .text5-1-4').addClass('animated fadeInUp');
			}, 1500);
			setTimeout(function() {
				$('.sec5 .text5-1-5').addClass('animated fadeInUp');
			}, 2000);
			setTimeout(function() {
				$('.sec5 .text5-1-6').addClass('animated fadeInUp');
			}, 2500);
		};
	};
	textAn()
	$(window).on('scroll', function() {
		textAn();
	});
});