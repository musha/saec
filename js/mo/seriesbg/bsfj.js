jQuery(function($) {
	// sec1 swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		initialSlide: 1,
		loop: true,
		slidesPerView: 'auto',
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: -49, /* 值越小两边的slide越靠近中间的slide */
            depth: 130,  /* 值越大两边的slide越小 */
            modifier: 4, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		onSlideChangeEnd: function(swiper) {
			// console.log(swiper.realIndex);
			// 改变色块
			$('.sec1 .color-item' + (swiper.realIndex + 1)).addClass('active').siblings().removeClass('active');
	    }
	});

	// 180 check
	$.prod180An('box-bsfj', $('#box-bsfj'), '../3d/180/bg/FJ/0_', 0, 36);
	
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		$('.modal-bsfj').fadeIn(300);
	});

	// sec1 download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sec2 content
	$('.sec2 .btn-sleepmode-1').on('click', function() {
		$('.bg01,.btn-sleepmode-1,.sleepmodeclose').removeClass('show').addClass('hide');
		$('.bg02,.btn-sleepmode-2,.sleepmodeopen').removeClass('hide').addClass('show');
	});
	$('.sec2 .btn-sleepmode-2').on('click', function() {
		$('.bg02,.btn-sleepmode-2,.sleepmodeopen').removeClass('show').addClass('hide');
		$('.bg01,.btn-sleepmode-1,.sleepmodeclose').removeClass('hide').addClass('show');
	});

	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-text'));
		$.fadeUpAn($('.sec2 .content'));
        $.fadeUpAn($('.sec3 .box-text'));
        $.fadeUpAn($('.sec3 .box-img'));
        $.fadeUpAn($('.sec3 .box-cards'));
		$.fadeUpAn($('.sec4 .box-title4-1'));
		$.fadeUpAn($('.sec4 .box-title4-2'));
		$.fadeUpAn($('.sec5 .box-title5-1'));
		$.fadeUpAn($('.sec5 .box-title5-2'));
		$.fadeUpAn($('.sec5 .box-text5-1'));
		$.fadeUpAn($('.sec5 .box-text5-2'));
	}, 300);
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-text'));
		$.fadeUpAn($('.sec2 .content'));
		$.fadeUpAn($('.sec3 .box-text'));
		$.fadeUpAn($('.sec3 .box-img'));
		$.fadeUpAn($('.sec3 .box-cards'));
		$.fadeUpAn($('.sec4 .box-title4-1'));
		$.fadeUpAn($('.sec4 .box-title4-2'));
		$.fadeUpAn($('.sec5 .box-title5-1'));
		$.fadeUpAn($('.sec5 .box-title5-2'));
		$.fadeUpAn($('.sec5 .box-text5-1'));
		$.fadeUpAn($('.sec5 .box-text5-2'));
	});

	// sec5
    function windAn() {
		if($.visionAn($('.sec5 .box-effect'), 0.83, 1)) {
            $('.sec5 .box-wind').animate({height: '293.97%'}, 3000);
		};
	}
	windAn();
	$(window).on('scroll', function() {
		windAn();
	});
});