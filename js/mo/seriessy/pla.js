jQuery(function($) {
	// sec1 download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	})

	// // sec3 box-btn-check
	$('.sec4 .box-btn-check img').on('click', function() {
		$('.modal-swt1').fadeIn(300);
	});
		$('.sec5 .box-btn-check img').on('click', function() {
		$('.modal-swt2').fadeIn(300);
	});



	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec2 .box-title'));
		$.fadeUpAn($('.sec2 .divider'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-1'));
		// $.fadeUpAn($('.sec2 .box-prod .box-prod2-2'));
		// $.fadeUpAn($('.sec2 .box-prod .box-prod2-3'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-4'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-5'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-6'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-7'));
		$.fadeUpAn($('.sec3 .box-subtitle3-1'));
		$.fadeUpAn($('.sec3 .box-subtitle3-2'));
		$.fadeUpAn($('.sec3 .box-subtitle3-3'));
		$.fadeUpAn($('.sec3 .box-prod'));
		$.fadeUpAn($('.sec4 .box-title'));
		$.fadeUpAn($('.sec4 .box-subtitle'));
		$.fadeUpAn($('.sec4 .box-btn-check'));
		$.fadeUpAn($('.sec5 .box-title'));
		$.fadeUpAn($('.sec5 .box-subtitle'));
		// $.fadeUpAn($('.sec5 .box-btn-check'));
		
	}, 300);
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec2 .box-title'));
		$.fadeUpAn($('.sec2 .divider'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-1'));
		// $.fadeUpAn($('.sec2 .box-prod .box-prod2-2'));
		// $.fadeUpAn($('.sec2 .box-prod .box-prod2-3'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-4'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-5'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-6'));
		$.fadeUpAn($('.sec2 .box-prod .box-prod2-7'));
		$.fadeUpAn($('.sec3 .box-subtitle3-1'));
		$.fadeUpAn($('.sec3 .box-subtitle3-2'));
		$.fadeUpAn($('.sec3 .box-subtitle3-3'));
		$.fadeUpAn($('.sec3 .box-prod'));
		$.fadeUpAn($('.sec4 .box-title'));
		$.fadeUpAn($('.sec4 .box-subtitle'));
		$.fadeUpAn($('.sec4 .box-btn-check'));
		$.fadeUpAn($('.sec5 .box-title'));
		$.fadeUpAn($('.sec5 .box-subtitle'));
		// $.fadeUpAn($('.sec5 .box-btn-check'));
		
	});


	// sec10
	var swiper10 = new Swiper('.swiper10', {
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect: 'coverflow',
		coverflow: {
            rotate: 0,
            stretch: -4, /* 值越小两边的slide越靠近中间的slide */
            depth: 0,  /* 值越大两边的slide越小 */
            modifier: 9, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		pagination: '.swiper-pagination10',
	});

});