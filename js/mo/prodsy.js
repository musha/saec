jQuery(function($) {
    // 淡入动画
    setTimeout(function() {
        // $.fadeUpAn($('.sec1 .box-title1'));
        // $.fadeUpAn($('.sec1 .text1'));
        // $.fadeUpAn($('.sec1 .btn-more'));
        $.fadeUpAn($('.sec2 .box-title2'));
        $.fadeUpAn($('.sec2 .text2'));
        $.fadeUpAn($('.sec2 .btn-more'));
        $.fadeUpAn($('.sec3 .box-title3'));
        $.fadeUpAn($('.sec3 .text3'));
        $.fadeUpAn($('.sec3 .btn-more'));
        // scroll
        $(window).on('scroll', function() {
            // $.fadeUpAn($('.sec1 .box-title1'));
            // $.fadeUpAn($('.sec1 .text1'));
            // $.fadeUpAn($('.sec1 .btn-more'));
            $.fadeUpAn($('.sec2 .box-title2'));
            $.fadeUpAn($('.sec2 .text2'));
            $.fadeUpAn($('.sec2 .btn-more'));
            $.fadeUpAn($('.sec3 .box-title3'));
            $.fadeUpAn($('.sec3 .text3'));
            $.fadeUpAn($('.sec3 .btn-more'));
        });
    }, 300);
});