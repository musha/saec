jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		// $.fadeUpAn($('.sec1 .box-title1'));
		// $.fadeUpAn($('.sec1 .box-swiper'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-swiper'));
		// scroll
		$(window).on('scroll', function() {
			// $.fadeUpAn($('.sec1 .box-title1'));
			// $.fadeUpAn($('.sec1 .box-swiper'));
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .box-swiper'));
		});
	}, 300);

	// sec1
	// swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		loop: true,
		slidesPerView: 'auto',
		spaceBetween: 28,
		pagination: '.sec1 .swiper-pagination'
	}); 

	// sec2 
	// swiper
	var swiper2 = new Swiper('.sec2 .swiper2', {
		spaceBetween: 28,
		pagination: '.sec2 .swiper-pagination'
	});
});