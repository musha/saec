jQuery(function($) {
    // 筛选条件
    var screenAside = {
        func1: 0,
        func2: 0,
        func3: 0,
        cons1: 0,
        cons2: 0,
        cons3: 0,
        feat1: 0,
        feat2: 0,
        areatype1: 0,
        areatype2: 0,
        area1: 0,
        area2: 0,
        area3: 0,
        area4: 0,
        area5: 0,
        area6: 0
    };

    // sec1
    // 获取产品分页
    var canGo = 1; // 默认可以跳转
    function getProd(func, cons, feat, areatype, area, pagenum) {
        if(func == null || func == undefined) {
            func == '';
        };
        if(cons == null || cons == undefined) {
            cons == '';
        };
        if(feat == null || feat == undefined) {
            feat == '';
        };
        if(areatype == null || areatype == undefined) {
            areatype = 0;
        };
        if(area == null || area == undefined) {
            area == '';
        };
        if(pagenum == null || pagenum == undefined) {
            pagenum = 1;
        };
        $('.sec1 .pagin').pagination({
            beforePaging: function() {
                // 正在调接口，不能重复调取
                $('.sec1 .modal-loading').fadeIn(300);
                $('.modal-screen .btn-reset').prop('disabled', true);
                $('.modal-screen .btn-screen').prop('disabled', true);
            },
            // url: 'http://saec.gypserver.com/service/api/Home/GetChannelProducts',
            dataSource: 'http://saec.gypserver.com/service/api/Home/GetChannelProducts',
            ajax: {
                type: 'get',
                // contentType: 'application/json',
                data: {
                    cnl: 4,
                    func: func,
                    consume: cons,
                    feature: feat,
                    areaType: areatype,
                    area: area,
                    category: '家用空调,柜式空调',
                    pType: 1,
                    page: pagenum,
                    pageSize: 12
                },
                // async: false,
                // cache: false,
                complete: function(data) {
                    // 调取完毕
                    $('.sec1 .modal-loading').fadeOut(300);
                    $('.modal-screen .btn-reset').prop('disabled', false);
                    $('.modal-screen .btn-screen').prop('disabled', false);
                }
            },
            // 参数别名
            alias: {
                pageNumber: 'page'
            },
            locator: 'data.data',
            pageRange: 1,
            pageSize: 12,
            // hideWhenLessThanOnePage: true,
            totalNumberLocator: function(res) {
                return res.data.total;
            },
            callback: function (data, pagination) {
                $('.sec1 .list-prod').empty();
                $('.sec1 .tip-error').addClass('hide');
                var len = data.length;
                if(len > 0) {
                    // 有数据
                    $('.sec1 .tip-no').addClass('hide');
                    for(var i = 0; i < len; i++) {
                        var url = data[i].linkUrl,
                            target = '_self';
                        if(url.length == 0) {
                            // 没有链接
                            url = '#';
                        } else {
                            // 有链接且为内部链接
                            if(url.indexOf('http') < 0) {
                                url = 'm.' + url + '.html';
                            } else {
                                // 外部链接
                                target = '_blank';
                            };
                        };
                        var $a = $('<a href="' + url + '" target="' + target + '"><li class="prod-item rel"><div class="prod-view"><img class="full-w" src="http://saec.gypserver.com/' + data[i].imageUrl.substring(1) + '"></div><div class="prod-name text-center">' + data[i].title + '</div></li></a>');
                            $('.sec1 .list-prod').append($a);
                    };
                } else {
                    // 无数据
                    $('.sec1 .tip-no').removeClass('hide');
                };
            },
            formatAjaxError: function(jqXHR, textStatus, errorThrown) {
                // 错误提示
                $('.sec1 .tip-error').removeClass('hide');
            },
            afterPageOnClick: function() {
                canGo = 0;
            },
            afterPreviousOnClick: function() {
                canGo = 0;
            },
            afterNextOnClick: function() {
                canGo = 0;
            },
            afterPaging: function(page) {
                if(canGo == 0) {
                    // 不可以跳转
                    sessionStorage.setItem('currentPage', JSON.stringify(page));
                } else {
                    // 可以跳转
                    if(JSON.parse(sessionStorage.getItem('currentPage')) != null || JSON.parse(sessionStorage.getItem('currentPage')) != undefined) {
                        if(page != JSON.parse(sessionStorage.getItem('currentPage'))) {
                            $('.sec1 .pagin').pagination(JSON.parse(sessionStorage.getItem('currentPage')));
                        };
                    };
                };
            }
        });
    };

    // 是否有存储筛选条件
    if(JSON.parse(sessionStorage.getItem('screenSelect')) != null || JSON.parse(sessionStorage.getItem('screenSelect')) != undefined) {
        // console.log('来自智能选型');
        sessionStorage.removeItem('screenAsideGsModel');
        sessionStorage.removeItem('currentPage');
        var screenSelect = JSON.parse(sessionStorage.getItem('screenSelect'));
        sessionStorage.setItem('screenSelectNew', JSON.stringify(screenSelect));
        var screenSelectNew = JSON.parse(sessionStorage.getItem('screenSelectNew'));
        sessionStorage.removeItem('screenSelect');
        setTimeout(function() {
            var l = $('.modal-screen .select-item').length;
            for(var i = 0; i < l; i++) {
                if($($('.modal-screen .select-item')[i]).data('value') == screenSelect['areatype'] || $($('.modal-screen .select-item')[i]).data('value') == screenSelect['area']) {
                    $($('.modal-screen .select-item')[i]).addClass('selected');
                    screenAside[$($('.modal-screen .select-item')[i]).attr('id')] = 1;
                    sessionStorage.setItem('screenAsideGsModel', JSON.stringify(screenAside));
                };
            };
            getProd('', '', '', screenSelectNew['areatype'], screenSelectNew['area'], 1);
        }, 300);
    } else {
        // console.log('刷新');
        if(JSON.parse(sessionStorage.getItem('screenAsideGsModel')) != null || JSON.parse(sessionStorage.getItem('screenAsideGsModel')) != undefined) {
            // 已有筛选
            screenAside = JSON.parse(sessionStorage.getItem('screenAsideGsModel'));
            $.each(screenAside, function(key ,value) {
                if(value == 0) {
                    $('#' + key).removeClass('selected');
                } else {
                    $('#' + key).addClass('selected');
                };
            });
            setTimeout(function() {
                var func = screenFun(func, $('.modal-screen .item-function')),
                cons = screenFun(cons, $('.modal-screen .item-consume')),
                feat = screenFun(feat, $('.modal-screen .item-feature')),
                area = screenFun(area, $('.modal-screen .item-area')),
                areatype = screenFun(areatype, $('.modal-screen .item-areatype'));
                getProd(func, cons, feat, areatype, area, 1);
            }, 300);
        } else {
            // 没有筛选
            sessionStorage.removeItem('currentPage');
            getProd('', '', '', 0, 0, 1);
        };
    };

    // 筛选弹窗
    $('.sec1 .box-btns').on('click', function() {
        $('.modal-screen').fadeIn(300);
    });

    // 筛选效果
    $('.modal-screen .select-item').on('click', function() {
        if($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            $(this).addClass('selected');
        };
    });

    // 筛选函数
    function screenFun(name, el) {
        if(el.find('.selected').length > 0) {
            // 选了
            var arr = [];
            var len = el.find('.selected').length;
            for(var i = 0; i < len; i++) {
                arr.push($(el.find('.selected')[i]).data('value'));
            };
            name = arr.join(',');
        } else {
            // 没选
            name = '';
        };
        return name;
    };
    // 筛选提交
    $('.modal .btn-screen').on('click', function() {
        $('.modal-screen').fadeOut(300);
        var func,
            cons,
            feat,
            areatype,
            area;
        func = screenFun(func, $('.modal-screen .item-function'));
        cons = screenFun(cons, $('.modal-screen .item-consume'));
        feat = screenFun(feat, $('.modal-screen .item-feature'));
        areatype = screenFun(areatype, $('.modal-screen .item-areatype'));
        area = screenFun(area, $('.modal-screen .item-area'));
        sessionStorage.removeItem('currentPage');
        getProd(func, cons, feat, areatype, area, 1);
        setTimeout(function() {
            var len = $('.modal-screen .select-item').length;
            for(var i = 0; i < len; i++) {
                if($($('.modal-screen .select-item')[i]).hasClass('selected')) {
                    screenAside[$($('.modal-screen .select-item')[i]).attr('id')] = 1;
                } else {
                    screenAside[$($('.modal-screen .select-item')[i]).attr('id')] = 0;
                };
            };
            sessionStorage.setItem('screenAsideGsModel', JSON.stringify(screenAside));
        }, 300);
    });

    // 重置
    $('.modal .btn-reset').on('click', function() {
        $('.modal .select-item').removeClass('selected');
    });
});