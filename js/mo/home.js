jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		// $.fadeUpAn($('.sec1 .box-title1'));
		// $.fadeUpAn($('.sec1 .box-other'));
		// $.fadeUpAn($('.sec1 .box-zykt'));
		$.fadeUpAn($('.sec2 .box-title2-1'));
		$.fadeUpAn($('.sec2 .box-player'));
		$.fadeUpAn($('.sec2 .box-title2-2'));
		$.fadeUpAn($('.sec2 .btn-more-l'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .btn-more-l'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .btn-more-l'));
		// scroll
		$(window).on('scroll', function() {
			// $.fadeUpAn($('.sec1 .box-title1'));
			// $.fadeUpAn($('.sec1 .box-other'));
			// $.fadeUpAn($('.sec1 .box-zykt'));
			$.fadeUpAn($('.sec2 .box-title2-1'));
			$.fadeUpAn($('.sec2 .box-player'));
			$.fadeUpAn($('.sec2 .box-title2-2'));
			$.fadeUpAn($('.sec2 .btn-more-l'));
			$.fadeUpAn($('.sec3 .box-title3'));
			$.fadeUpAn($('.sec3 .btn-more-l'));
			$.fadeUpAn($('.sec4 .box-title4'));
			$.fadeUpAn($('.sec4 .btn-more-l'));
		});
	}, 300);

	// sec0 
	// swiper
	var swiper0 = new Swiper('.sec0 .swiper0', {
		loop: true,
		autoplayDisableOnInteraction : false,
		effect: 'fade',
		prevButton:'.sec0 .swiper-button-prev',
		nextButton:'.sec0 .swiper-button-next'
	});

	// player
	var playerboss,
		playerhome,
		playerfz,
		playersjl,
		playerbrand;
	var getPlayerboss = setInterval(function() {
		playerboss = $('#ifrboss').contents().find('#playerboss');
		if(playerboss != undefined && playerboss != null) {
			if(playerboss.length > 0) {
				clearInterval(getPlayerboss);
				$('.sec0 .btn-play1').on('click', function() {
					$('.modal-boss').fadeIn(300);
					playerboss[0].play();
				});
			};
		};
	}, 300);
	var getPlayerhome = setInterval(function() {
		playerhome = $('#ifrhome').contents().find('#playerhome');
		if(playerhome != undefined && playerhome != null) {
			if(playerhome.length > 0) {
				clearInterval(getPlayerhome);
				$('.sec0 .btn-play2').on('click', function() {
					$('.modal-home').fadeIn(300);
					playerhome[0].play();
				});
			};
		};
	}, 300);
	var getPlayerfz = setInterval(function() {
		playerfz = $('#ifrfz').contents().find('#playerfz');
		if(playerfz != undefined && playerfz != null) {
			if(playerfz.length > 0) {
				clearInterval(getPlayerfz);
				$('.sec0 .btn-play3').on('click', function() {
					$('.modal-fz').fadeIn(300);
					playerfz[0].play();
				});
			};
		};
	}, 300);
	var getPlayersjl = setInterval(function() {
		playersjl = $('#ifrsjl').contents().find('#playerjl');
		if(playersjl != undefined && playersjl != null) {
			if(playersjl.length > 0) {
				clearInterval(getPlayersjl);
				$('.sec0 .btn-play4').on('click', function() {
					$('.modal-sjl').fadeIn(300);
					playersjl[0].play();
				});
			};
		};
	}, 300);
	var getPlayerbrand = setInterval(function() {
		playerbrand = $('#ifrbrand').contents().find('#playerbrand');
		if(playerbrand != undefined && playerbrand != null) {
			if(playerbrand.length > 0) {
				clearInterval(getPlayerbrand);
				$('.sec2 .btn-player').on('click', function() {
					$('.modal-brand').fadeIn(300);
					playerbrand[0].play();
				});
			};
		};
	}, 300);

	// close player
	$('.modal .btn-close').on('click', function() {
		if($(this).parent().next().contents().find('video')[0].paused) {
			//
		} else {
			$(this).parent().next().contents().find('video')[0].pause();
		};
		$('.modal').fadeOut(300);
	});

	// hover
	$('.sec0 .btn-play').hover(function() {
		$(this).attr('src', '../img/mo/home/btn-play-active.png');
	}, function() {
		$(this).attr('src', '../img/mo/home/btn-play.png');
	});
	$('.btn-more-s').on('touchstart', function() {
		$(this).attr('src', '../img/mo/btn-more-s-active.png');
	});
	$('.box-zykt img').on('touchstart', function() {
		$(this).attr('src', '../img/mo/home/btn-zykt-active.png');
	});
	$('.btn-more-l').on('touchstart', function() {
		$(this).attr('src', '../img/mo/home/btn-more-l-active.png');
	});
});