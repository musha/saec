jQuery(function($) {
    sec1();
    sec3();
    sec4();
    sec6();

    // scroll
    $(window).on('scroll', function() {
        $.fadeUpAn($('.sec3 .titleicon'));
        $.fadeUpAn($('.sec3 .title'));
        $.fadeUpAn($('.sec3 .text1'));
        $.fadeUpAn($('.sec3 .text2'));
        $.fadeUpAn($('.sec3 .text3'));
        $.fadeUpAn($('.sec3 .select'));
        $.fadeUpAn($('.sec4 .titleicon'));
        $.fadeUpAn($('.sec4 .title'));
        $.fadeUpAn($('.sec6 .titleicon'));
    })
})

function sec1() {
    // sec1 swiper
    var swiper1 = new Swiper('.sec1 .swiper1', {
        loop: true,
        slidesPerView: 'auto',
        centeredSlides : true,
        pagination: '.sec1 .swiper-pagination',
        effect: 'coverflow',
        coverflow: {
            rotate: 0,
            stretch: 36, /* 值越小两边的slide越靠近中间的slide */
            depth: 50, /* 值越大两边的slide越小 */
            modifier: 4, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><div class="color' + (index + 1) + '"></div></div>';
        },
        onSlideChangeEnd: function(swiper) {
            // console.log('---', swiper.realIndex);
            if (swiper.realIndex == 0) {
                $('.sec1 .btn-360').attr('href', '../3d/360/gs/XFJ SXFJ/XFJ/index.html');
            }
            if (swiper.realIndex == 1) {
                $('.sec1 .btn-360').attr('href', '../3d/360/gs/XFJ SXFJ/SXFJ/index.html');
            }
        }
    });

    // 180 check
    $.prod180An('box-xfj', $('#box-xfj'), '../3d/180/gs/XFJ SXFJ/XFJ/XF00', 0, 36);
    $.prod180An('box-sxfj', $('#box-sxfj'), '../3d/180/gs/XFJ SXFJ/SXFJ/0_', 0, 36);

    // 180 modal
    $('.sec1 .btn-180').on('click', function() {
        switch (parseInt(swiper1.realIndex)) {
            case 0:
                $('.modal-xfj').fadeIn(300);
                break;
            case 1:
                $('.modal-sxfj').fadeIn(300);
                break;
            default:
                //
        };
    });
    // sec1 download
    $('.sec1 .box-download img').on('click', function() {
        $('.modal-download').fadeIn(300);
    });

    // close modal
    $('.modal .btn-close').on('click', function() {
        $('.modal').fadeOut(300);
    });
}

function sec3() {
    // sec3 swiper
    var swiper3 = new Swiper('.sec3 .swiper3', {
        initialSlide: 0,
        loop: false,
        effect: 'fade',
        fade: {
            crossFade: false,
        },
        onSlideChangeEnd: function(swiper) {
            if (swiper.realIndex == 0) {
                $('.sec3 .content .text1 img').attr('src', '../img/mo/seriesgs/xfj_sxfj/section3_text1_cold.png')
            }
            if (swiper.realIndex == 1) {
                $('.sec3 .content .text1 img').attr('src', '../img/mo/seriesgs/xfj_sxfj/section3_text1_hot.png')
            }
        }
    });
    $('.selectcold').on('click', function() {
        if (swiper3.activeIndex == 0) {
            swiper3.slideNext()
        }
    })
    $('.selecthot').on('click', function() {
        if (swiper3.activeIndex == 1) {
            swiper3.slidePrev()
        }
    })
}

function sec4() {
    // sec4 swiper
    var swiper4 = new Swiper('.sec4 .swiper4', {
        initialSlide: 0,
        loop: true,
        pagination: '.swiper4-pagination',
        onSlideChangeEnd: function(swiper) {
            switch (swiper.realIndex) {
                case 0:
                    $('.swiper4_info img').attr('src', "../img/mo/seriesgs/xfj_sxfj/section4_info_12.png")
                    break;
                case 1:
                    $('.swiper4_info img').attr('src', "../img/mo/seriesgs/xfj_sxfj/section4_info_qfw.png")
                    break;
            }
        }
    });
    $('.sec4 .prev').on('click', function() {
        swiper4.slidePrev()
    })
    $('.sec4 .next').on('click', function() {
        swiper4.slideNext()
    })
}

function sec6() {
    var swiper6 = new Swiper('.sec6 .swiper6', {
        initialSlide: 0,
        loop: true,
        pagination: '.swiper6-pagination',
        onSlideChangeEnd: function(swiper) {
            // console.log(swiper.realIndex);
        }
    });
}