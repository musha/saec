jQuery(function ($) {
    sec1();
    sec2();
    sec3();
    sec4();

    $(window).on('scroll', function () {
        $.fadeUpAn($('.sec2 .titleicon'));
        $.fadeUpAn($('.sec2 .title'));
        $.fadeUpAn($('.sec2 .text2-1'));
        $.fadeUpAn($('.sec2 .text2-2'));
        $.fadeUpAn($('.sec2 .text2-3'));
        $.fadeUpAn($('.sec2 .text2-4'));
        $.fadeUpAn($('.sec2 .swiper-pagination'));
        $.fadeUpAn($('.sec3 .titleicon'));
        $.fadeUpAn($('.sec3 .title'));
        $.fadeUpAn($('.sec3 .swiper3-pagination'));
        $.fadeUpAn($('.sec3 .swiper3_info'));
        $.fadeUpAn($('.sec3 .prev'));
        $.fadeUpAn($('.sec3 .next'));
        $.fadeUpAn($('.sec4 .titleicon'));
        $.fadeUpAn($('.sec4 .swiper4'));
        $.fadeUpAn($('.sec4 .swiper4-pagination'));
    })
})

function sec1() {
    // sec1 swiper
    var swiper1 = new Swiper('.sec1 .swiper1', {
        loop: true,
        slidesPerView: 'auto',
        centeredSlides : true,
        pagination: '.sec1 .swiper-pagination',
        effect: 'coverflow',
        coverflow: {
            rotate: 0,
            stretch: 36, /* 值越小两边的slide越靠近中间的slide */
            depth: 50, /* 值越大两边的slide越小 */
            modifier: 4, /* 值越小两边的slide越小 */
            slideShadows: false
        },
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><div class="color' + (index + 1) + '"></div></div>';
        },
        onSlideChangeEnd: function (swiper) {
            if (swiper.realIndex == 0) {
                $('.sec1 .btn-360').attr('href', '../3d/360/gs/VJ MVJ/VJ/index.html');
            }
            if (swiper.realIndex == 1) {
                $('.sec1 .btn-360').attr('href', '../3d/360/gs/VJ MVJ/MVJ/index.html');
            }
        }
    });

    // 180 check
    $.prod180An('box-vj', $('#box-vj'), '../3d/180/gs/VJ MVJ/VJ/0_', 0, 36);
    $.prod180An('box-mvj', $('#box-mvj'), '../3d/180/gs/VJ MVJ/MVJ/0_', 0, 36);

    // 180 modal
    $('.sec1 .btn-180').on('click', function () {
        switch (parseInt(swiper1.realIndex)) {
            case 0:
                $('.modal-vj').fadeIn(300);
                break;
            case 1:
                $('.modal-mvj').fadeIn(300);
                break;
            default:
                //
        };
    });
    // sec1 download
    $('.sec1 .box-download img').on('click', function () {
        $('.modal-download').fadeIn(300);
    });

    // close modal
    $('.modal .btn-close').on('click', function () {
        $('.modal').fadeOut(300);
    });
}

function sec2() {
    var typeArr = ['制冷', '制热'];
    var swiper2 = new Swiper('.sec2 .swiper2', {
        initialSlide: 0,
        loop: false,
        effect: 'fade',
        pagination: '.sec2 .swiper-pagination',
        paginationClickable: true,
        paginationBulletRender: function (swiper, index, className) {
            return '<div class="' + className + '"><span>' + typeArr[index] + '</span></div>';
        },
        onSlideChangeEnd: function(swiper) {
            switch(swiper.realIndex) {
                case 0:
                    $('.sec2 .swiper2 .swiper-pagination').addClass('colorcold').removeClass('colorhot');
                    break;
                case 1:
                    $('.sec2 .swiper2 .swiper-pagination').addClass('colorhot').removeClass('colorcold');
                    break;
                default:
                    //
            };
        }
    });
    $('.selectcold').on('click', function () {
        if (swiper2.activeIndex == 0) {
            swiper2.slideNext()
        }
    })
    $('.selecthot').on('click', function () {
        if (swiper2.activeIndex == 1) {
            swiper2.slidePrev()
        }
    })
}

function sec3() {
    var swiper3 = new Swiper('.sec3 .swiper3', {
        initialSlide: 0,
        loop: true,
        effect: 'fade',
        pagination: '.swiper3-pagination',
        onSlideChangeEnd: function (swiper) {
            if (swiper.realIndex == 0) {
                $('.sec3 .swiper3_info img').attr('src', '../img/mo/seriesgs/vj_mvj/text3-1.png')
            }
            if (swiper.realIndex == 1) {
                $('.sec3 .swiper3_info img').attr('src', '../img/mo/seriesgs/vj_mvj/text3-2.png')
            }
        }
    });
    $('.sec3 .prev').on('click', function () {
        swiper3.slidePrev()
    })
    $('.sec3 .next').on('click', function () {
        swiper3.slideNext()
    })
}

function sec4() {
    var swiper4 = new Swiper('.sec4 .swiper4', {
        initialSlide: 0,
        loop: true,
        pagination: '.swiper4-pagination',
    });
}
