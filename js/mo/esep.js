jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		//
		// scroll
		$(window).on('scroll', function() {
			//
		});
	}, 300);

    // 节能
    $('.sec1 .slide-title').on('click', function() {
        $(this).next().stop(true, true).slideToggle();
        $(this).find('.divider2').toggleClass('hide');
        if($(this).find('.divider2').hasClass('hide')) {
            // 当前展开 其他折叠
            $(this).parent().siblings().each(function() {
                if($(this).find('.divider2').hasClass('hide')) {
                    $(this).find('.slide-content').stop(true, true).slideUp();
                    $(this).find('.divider2').removeClass('hide');
                };
            });
        };
    });
});