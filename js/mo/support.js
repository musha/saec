jQuery(function($) {
    // 来自销售支持
    if(JSON.parse(sessionStorage.getItem('store')) != null || JSON.parse(sessionStorage.getItem('store')) != undefined) {
        setTimeout(function() {
            $('html, body').animate({
                scrollTop: $('.sec1').offset().top * 0.72
            }, 400);
            sessionStorage.removeItem('store');
        }, 300);
    };

    // 来自售后支持
    if(JSON.parse(sessionStorage.getItem('service')) != null || JSON.parse(sessionStorage.getItem('service')) != undefined) {
        setTimeout(function() {
            $('html, body').animate({
                scrollTop: $('.sec2').offset().top
            }, 400);
            sessionStorage.removeItem('service');
        }, 300);
    };

    // hover
    $('.sec3 .btn-policy').on('touchstart', function() {
        $(this).attr('src', '../img/mo/support/btn-policy-active.png');
    });

    $('.sec3 .btn-guide').on('touchstart', function() {
        $(this).attr('src', '../img/mo/support/btn-guide-active.png');
    });

    // 百度地图API功能
    var map = new BMap.Map('shopmap');    // 创建Map实例
    map.centerAndZoom('上海', 12);  // 初始化地图 设置中心点坐标和地图级别 默认上海

    // 添加带有定位的导航控件
    var navigationControl = new BMap.NavigationControl({
        // 靠左上角位置
        anchor: BMAP_ANCHOR_TOP_LEFT,
        // LARGE类型
        type: BMAP_NAVIGATION_CONTROL_LARGE,
        // 启用显示定位
        enableGeolocation: true
    });
    map.addControl(navigationControl);

    // 搜索
    var local = new BMap.LocalSearch(map, {
        renderOptions: {
            map: map,
            autoViewport: true 
        }
    });

    // 根据解析到的城市获取门店和网点
    function initCity(city, area) {
        $.ajax({
            type: 'get',
            url: 'http://saec.gypserver.com/service/api/Home/GetArea',
            dataType: 'json',
            // contentType: 'application/json',
            data: {
                cnl: 18,
                isAll: true
            },
            // async: false,
            cache: false,
            success: function(data) {
                // console.log(data);
                if(data.success) {
                    $('.sec1 .tip-error').addClass('hide');
                    // 根据城市名取对应的id
                    var len = data.data.data.length;
                    if(len > 0) {
                        var id;
                        for(var i = 0; i < len; i++) {
                            if(data.data.data[i].channelName == city) {
                                id = data.data.data[i].id;
                            };
                        };
                        if(id != null || id != undefined) {
                            // 有id
                            getStore(id);
                        };
                    };
                };
            },
            error: function() {
                // 错误提示
                $('.sec1 .tip-error').removeClass('hide');
            },
            complete: function(data) {
                // 调取完毕
            }
        });
        $.ajax({
            type: 'get',
            url: 'http://saec.gypserver.com/service/api/Home/GetArea',
            dataType: 'json',
            // contentType: 'application/json',
            data: {
                cnl: 17,
                isAll: true
            },
            // async: false,
            cache: false,
            success: function(data) {
                // console.log(data);
                if(data.success) {
                    $('.sec2 .tip-error').addClass('hide');
                    // 根据城市名取对应的id
                    var len = data.data.data.length;
                    if(len > 0) {
                        var id;
                        for(var i = 0; i < len; i++) {
                            if(data.data.data[i].channelName == area) {
                                id = data.data.data[i].id;
                            };
                        };
                        if(id != null || id != undefined) {
                            // 有id
                            getPoint(id);
                        };
                    };
                };
            },
            error: function() {
                // 错误提示
                $('.sec2 .tip-error').removeClass('hide');
            },
            complete: function(data) {
                // 调取完毕
            }
        });
    };

    // 获取位置
    var userPos = JSON.parse(sessionStorage.getItem('city'));
    if(userPos == null || userPos == undefined) {
        // 没有存储地址 判断是否已经申请过位置授权
        var mapStatus = JSON.parse(sessionStorage.getItem('status'));
        if(mapStatus == null || mapStatus == undefined) {
            // 没有申请授权 去获取授权
            var geolocation = new BMap.Geolocation();
            geolocation.getCurrentPosition(function(r) {
                if(this.getStatus() == BMAP_STATUS_SUCCESS) {
                    // 获取了位置
                    var mk = new BMap.Marker(r.point);
                    map.addOverlay(mk);
                    map.panTo(r.point);
                    // console.log('您的位置：' + r.point.lng + ',' + r.point.lat);
                    map.clearOverlays(); 
                    var new_point = new BMap.Point(r.point.lng, r.point.lat);
                    var marker = new BMap.Marker(new_point);  // 创建标注
                    map.addOverlay(marker);              // 将标注添加到地图中
                    setTimeout(function() {
                        // 地图定位到用户位置
                        map.panTo(new_point);
                    }, 300);
                    // 解析位置 获取城市
                    var geoc = new BMap.Geocoder();   
                    geoc.getLocation(r.point, function(rs){
                        var addComp = rs.addressComponents;
                        // console.log(addComp);
                        if(addComp.city != null || addComp.city != undefined) {
                            // 解析了地址 获得了城市名
                            sessionStorage.setItem('city', JSON.stringify(addComp));
                            $.ajax({
                                type: 'get',
                                url: 'http://saec.gypserver.com/service/api/Home/GetArea',
                                dataType: 'json',
                                // contentType: 'application/json',
                                data: {
                                    cnl: 18,
                                    isAll: true
                                },
                                // async: false,
                                cache: false,
                                success: function(data) {
                                    // console.log(data);
                                    if(data.success) {
                                        $('.sec1 .tip-error').addClass('hide');
                                        // 根据城市名取对应的id
                                        var len = data.data.data.length;
                                        if(len > 0) {
                                            var id;
                                            for(var i = 0; i < len; i++) {
                                                if(data.data.data[i].channelName == addComp.city) {
                                                    id = data.data.data[i].id;
                                                };
                                            };
                                            if(id != null || id != undefined) {
                                                // 有id
                                                getStore(id);
                                            } else {
                                                // 没有id 默认上海
                                                for(var i = 0; i < len; i++) {
                                                    if(data.data.data[i].channelName == '上海') {
                                                        id = data.data.data[i].id;
                                                    };
                                                };
                                                if(id != null || id != undefined) {
                                                    getStore(id);
                                                };
                                            };
                                        };
                                    };
                                },
                                error: function() {
                                    // 错误提示
                                    $('.sec1 .tip-error').removeClass('hide');
                                },
                                complete: function(data) {
                                    // 调取完毕
                                }
                            });
                            $.ajax({
                                type: 'get',
                                url: 'http://saec.gypserver.com/service/api/Home/GetArea',
                                dataType: 'json',
                                // contentType: 'application/json',
                                data: {
                                    cnl: 17,
                                    isAll: true
                                },
                                // async: false,
                                cache: false,
                                success: function(data) {
                                    // console.log(data);
                                    if(data.success) {
                                        $('.sec2 .tip-error').addClass('hide');
                                        // 根据城市名取对应的id
                                        var len = data.data.data.length;
                                        if(len > 0) {
                                            var id;
                                            for(var i = 0; i < len; i++) {
                                                if(data.data.data[i].channelName == addComp.district) {
                                                    id = data.data.data[i].id;
                                                };
                                            };
                                            if(id != null || id != undefined) {
                                                // 有id
                                                getPoint(id);
                                            } else {
                                                // 没有id 默认上海徐汇区
                                                for(var i = 0; i < len; i++) {
                                                    if(data.data.data[i].channelName == '徐汇') {
                                                        id = data.data.data[i].id;
                                                    };
                                                };
                                                if(id != null || id != undefined) {
                                                    getPoint(id);
                                                };
                                            };
                                        };
                                    };
                                },
                                error: function() {
                                    // 错误提示
                                    $('.sec2 .tip-error').removeClass('hide');
                                },
                                complete: function(data) {
                                    // 调取完毕
                                }
                            });
                            // initCity(addComp.city, addComp.district);
                        } else {
                            // 解析失败 默认上海
                            initCIty('上海', '徐汇');
                        };
                    }); 
                } else {
                    // 默认上海
                    initCity('上海', '徐汇');
                    // switch(this.getStatus()) {
                    //     case 0:
                    //         //
                    //         break;
                    //     case 1:
                    //         // 
                    //         break;
                    //     case 2:
                    //         alert('位置结果未知');
                    //         break;
                    //     case 3:
                    //         alert('导航结果未知');
                    //         break;
                    //     case 4:
                    //         alert('非法密钥');
                    //         break;
                    //     case 5:
                    //         alert('非法请求');
                    //         break;
                    //     case 6:
                    //         // alert('没有权限');
                    //         break;
                    //     case 7:
                    //         alert('服务不可用');
                    //         break;
                    //     case 8:
                    //         alert('超时');
                    //         break;
                    //     default:
                    //         //
                    // };
                };
                sessionStorage.setItem('status', JSON.stringify(this.getStatus()));
            },{enableHighAccuracy: true});
        } else {
            // 已经向用户申请过授权 但是没有获取到 默认上海
            initCity('上海', '徐汇');
        };
    } else {
        // 存储了地址
        initCity(userPos.city.substring(0,userPos.city.indexOf('市')), userPos.district.substring(0,userPos.city.indexOf('区')));
    };

    // 获取区域 未完成之前不能下拉选择
    function getArea(id, el, el1, el2, el3) {
        el1.prop('disabled', true);
        el2.prop('disabled', true);
        if(el3 != null || el3 != undefined) {
            el3.prop('disabled', true);
        };
        $.ajax({
            type: 'get',
            url: 'http://saec.gypserver.com/service/api/Home/GetArea',
            dataType: 'json',
            // contentType: 'application/json',
            data: {
                cnl: id
            },
            // async: false,
            cache: false,
            success: function(data) {
                // console.log(data);
                if(data.success) {
                    el.find('.default').siblings().remove();
                    $('.sec1 .tip-error').addClass('hide');
                    var len = data.data.data.length;
                    if(len > 0) {
                        for(var i = 0; i < len; i++) {
                            var $option = $('<option value="' + data.data.data[i].id + '">' + data.data.data[i].channelName + '</option>');
                            el.append($option);
                        };
                    };
                };
            },
            error: function() {
                // 错误提示
                $('.sec1 .tip-error').removeClass('hide');
            },
            complete: function(data) {
                // 调取完毕
                el1.prop('disabled', false);
                el2.prop('disabled', false);
                if(el3 != null || el3 != undefined) {
                    el3.prop('disabled', false);
                };
            }
        });
    };
    // 先是填充省份
    getArea(18, $('.sec1 .province'), $('.sec1 .province'), $('.sec1 .city'));

    // 获取店铺 未完成之前不能下拉选择
    function getStore(id) {
        $('.sec1 .province').prop('disabled', true);
        $('.sec1 .city').prop('disabled', true);
        $.ajax({
            type: 'get',
            url: 'http://saec.gypserver.com/service/api/Home/GetStores',
            dataType: 'json',
            // contentType: 'application/json',
            data: {
                cnl: id
            },
            // async: false,
            cache: false,
            success: function(data) {
                // console.log(data);
                if(data.success) {
                    $('.sec1 .swiper1 .swiper-wrapper').empty();
                    $('.sec1 .tip-error').addClass('hide');
                    var len = data.data.data.length;
                    if(len > 0) {
                        // 有数据
                        $('.sec1 .tip-no').addClass('hide');
                        var n = 0,   // 初始值
                            max = 5; // 每一页最多数量
                            page = Math.ceil(len/max), // 总页数
                            pageArr = [],  // 每一页的slide数
                            dataArr = []; // 数据组
                        for(var i = 0; i < page; i++) {
                            var $slide = $('<div class="swiper-slide"></div>');
                            pageArr.push($slide);
                        };
                        dataArr = data.data.data;
                        for(var i = 0; i < page; i++) {
                            var $ul = $('<ul class="box-result"></ul>');
                            for(var j = 0; j < max; j++) {
                                if(dataArr.length > 0) {
                                    var $li = $('<li class="res-item rel"><div class="res-title"><i class="icon-loc"></i><div>' + dataArr[0].title + '</div></div><div class="res-city">' + dataArr[0].province + ' ' + dataArr[0].city + '</div><div class="res-address">地址：<span>' + dataArr[0].address + '</span></div><div class="btn-pos abs"></div></li>');
                                    $ul.append($li);
                                    dataArr.shift();
                                };
                            };
                            pageArr[i].append($ul);
                            $('.sec1 .swiper1 .swiper-wrapper').append(pageArr[i]);
                        };
                        // swiper
                        var swiper1 = new Swiper('.sec1 .swiper1', {
                            autoplayDisableOnInteraction : false,
                            pagination: '.swiper-pagination1',
                            paginationType: 'fraction',
                            observer: true, //修改swiper自己或子元素时，自动初始化swiper
                            observeParents: false, //修改swiper的父元素时，自动初始化swiper
                            onSlideChangeEnd: function(swiper){
                                swiper.update();  
                            }
                        });
                    } else {
                        // 无数据
                        $('.sec1 .tip-no').removeClass('hide');
                    };
                };
            },
            error: function() {
                // 错误提示
                $('.sec1 .tip-error').removeClass('hide');
            },
            complete: function(data) {
                // 调取完成
                $('.sec1 .province').prop('disabled', false);
                $('.sec1 .city').prop('disabled', false);
                // 打开地图 门店定位
                $('.sec1 .box-result .btn-pos').on('click', function() {
                    var point;
                    if($(this).siblings('.res-address').find('span').text() != '') {
                        point = $(this).siblings('.res-address').find('span').text();
                    } else {
                        point = $(this).siblings('.res-title').find('div').text();
                    };
                    $('.modal-map').fadeIn(300);
                    local.search(point);
                    // map.panBy(-(($(window).width() * 450) / 1920), ($(window).width() * 500) / 1920);
                });
                // 关闭地图
                $('.modal-map .btn-close').on('touchend', function() {
                    $('.modal-map').fadeOut(300);
                });
            }
        });
    };
    
    // 填充城市
    $('.sec1 .province').on('change propertychange', function() {
        if($(this).val() != '') {
            // 有值
            $('.sec1 .city').prop('disabled', false);
            getArea($(this).val(), $('.sec1 .city'), $('.sec1 .province'), $('.sec1 .city'));
        } else {
            // 无值
            $('.sec1 .city .default').siblings().remove();
        };
    });

    // 城市定位
    $('.sec1 .city').on('change propertychange', function() {
        if($(this).val() != '') {
            getStore($('.sec1 .city').val());
        };
    });

    // sec2 
    // 先获取区域
    getArea(17, $('.sec2 .province'), $('.sec2 .province'), $('.sec2 .city'), $('.sec2 .area'));

    // 获取网点 未完成之前不能下拉选择
    function getPoint(id) {
        $('.sec2 .province').prop('disabled', true);
        $('.sec2 .city').prop('disabled', true);
        $.ajax({
            type: 'get',
            url: 'http://saec.gypserver.com/service/api/Home/GetStores',
            dataType: 'json',
            // contentType: 'application/json',
            data: {
                cnl: id
            },
            // async: false,
            cache: false,
            success: function(data) {
                // console.log(data);
                if(data.success) {
                    $('.sec2 .swiper2 .swiper-wrapper').empty();
                    $('.sec2 .tip-error').addClass('hide');
                    var len = data.data.data.length;
                    if(len > 0) {
                        // 有数据
                        $('.sec2 .tip-no').addClass('hide');
                        var n = 0,   // 初始值
                            max = 3; // 每一页最多数量
                            page = Math.ceil(len/max), // 总页数
                            pageArr = [],  // 每一页的slide数
                            dataArr = []; // 数据组
                        for(var i = 0; i < page; i++) {
                            var $slide = $('<div class="swiper-slide"></div>');
                            pageArr.push($slide);
                        };
                        dataArr = data.data.data;
                        for(var i = 0; i < page; i++) {
                            var $ul = $('<ul class="box-result"></ul>');
                            for(var j = 0; j < max; j++) {
                                if(dataArr.length > 0) {
                                    var $li = $('<li class="res-item"><div class="res-title"><i class="icon-loc"></i><div>' + dataArr[0].title + '</div></div><div class="res-city">' + dataArr[0].province + ' ' + dataArr[0].city + ' ' + dataArr[0].county + '</div><div class="res-more"><div class="res-address">地址：' + dataArr[0].address + '</div><div class="res-tel">电话：' + dataArr[0].phone + '</div><div class="res-code">邮编：' + dataArr[0].postCode + '</div></div></li>');
                                    $ul.append($li);
                                    dataArr.shift();
                                };
                            };
                            pageArr[i].append($ul);
                            $('.sec2 .swiper2 .swiper-wrapper').append(pageArr[i]);
                        };
                        // swiper
                        var swiper2 = new Swiper('.sec2 .swiper2', {
                            autoplayDisableOnInteraction : false,
                            pagination: '.swiper-pagination2',
                            paginationType: 'fraction',
                            observer: true, //修改swiper自己或子元素时，自动初始化swiper
                            observeParents: false, //修改swiper的父元素时，自动初始化swiper
                            onSlideChangeEnd: function(swiper){
                                swiper.update();  
                            }
                        });
                    } else {
                        // 无数据
                        $('.sec2 .tip-no').removeClass('hide');
                    }
                };
            },
            error: function() {
                // 错误提示
                $('.sec2 .tip-error').removeClass('hide');
            },
            complete: function(data) {
                // 调取完成
                $('.sec2 .province').prop('disabled', false);
                $('.sec2 .city').prop('disabled', false);
            }
        });
    };

    // 填充城市
    $('.sec2 .province').on('change propertychange', function() {
        if($(this).val() != '') {
            // 有值
            $('.sec2 .city').prop('disabled', false);
            getArea($(this).val(), $('.sec2 .city'), $('.sec2 .province'), $('.sec2 .city'), $('.sec2 .area'));
        } else {
            // 无值
            $('.sec2 .city .default').siblings().remove();
        };
        $('.sec2 .area .default').siblings().remove();
    });

    // 填充区
    $('.sec2 .city').on('change propertychange', function() {
        if($(this).val() != '') {
            // 有值
            $('.sec2 .area').prop('disabled', false);
            getArea($(this).val(), $('.sec2 .area'), $('.sec2 .province'), $('.sec2 .city'), $('.sec2 .area'));
        } else {
            // 无值
            $('.sec2 .area .default').siblings().remove();
        };
    });

    // 市区定位
    $('.sec2 .area').on('change propertychange', function() {
        if($(this).val() != '') {
            getPoint($('.sec2 .area').val());
        };
    });
});