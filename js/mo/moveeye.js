jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		// $.fadeUpAn($('.sec0 .box-title0'));
		// $.fadeUpAn($('.sec0 .box-player'));
		// $.fadeUpAn($('.sec0 .box-swiper'));
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .year1994'));
		$.fadeUpAn($('.sec1 .year2005'));
		$.fadeUpAn($('.sec1 .year2014'));
		$.fadeUpAn($('.sec1 .year2016'));
		$.fadeUpAn($('.sec1 .year2019'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .content2-1'));
		$.fadeUpAn($('.sec2 .content2-2'));
		$.fadeUpAn($('.sec2 .content2-3'));
		// scroll
		$(window).on('scroll', function() {
			// $.fadeUpAn($('.sec0 .box-title0'));
			// $.fadeUpAn($('.sec0 .box-player'));
			// $.fadeUpAn($('.sec0 .box-swiper'));
			$.fadeUpAn($('.sec1 .box-title1'));
			$.fadeUpAn($('.sec1 .year1994'));
			$.fadeUpAn($('.sec1 .year2005'));
			$.fadeUpAn($('.sec1 .year2014'));
			$.fadeUpAn($('.sec1 .year2016'));
			$.fadeUpAn($('.sec1 .year2019'));
			$.fadeUpAn($('.sec2 .box-title2'));
			$.fadeUpAn($('.sec2 .content2-1'));
			$.fadeUpAn($('.sec2 .content2-2'));
			$.fadeUpAn($('.sec2 .content2-3'));
		});
	}, 300);

	// sec0 
	// swiper
	var swiper0 = new Swiper('.sec0 .swiper0', {
		loop: true,
		slidesPerView: 'auto',
		spaceBetween: 28,
		pagination: '.sec0 .swiper-pagination'
	});

	// player
	var playerjl;
	var getPlayer = setInterval(function() {
		playerjl = $('#ifrjl').contents().find('#playerjl');
		if(playerjl != undefined && playerjl != null) {
			if(playerjl.length > 0) {
				clearInterval(getPlayer);
				$('.sec0 .btn-player').on('click', function() {
					$('.modal-jl').fadeIn(300);
					playerjl[0].play();
				});
			};
		};
	}, 300);
	
	// close player
	$('.modal .btn-close').on('click', function() {
		if($(this).parent().next().contents().find('video')[0].paused) {
			//
		} else {
			$(this).parent().next().contents().find('video')[0].pause();
		};
		$('.modal').fadeOut(300);
	});

	// 全域慧眼
	$('.sec2 .btn-check').on('click', function() {
		sessionStorage.setItem('selectFun', JSON.stringify($(this).data('func')));
	});

	// hover
	$('.btn-check').on('touchstart', function() {
		$(this).attr('src', '../img/mo/moveeye/btn-check-active.png');
	});
});